#基础工具包:
   ##权限:
   
   #####用户登录
   AdminAuth::login($userName,$userPassword,$userProjectId);返回token
   
   #####判断用户权限
   AdminAuth::checkUserAuth($path,$token);返回true、false
   
   #####获取当前用户所有权限
   AdminAuth::getAuth($token);返回树形结构权限

---
   ##基础功能
   
   #####上传文件
   UploadTool::uploadFile($file,$filename='');返回文件路径
   
   #####httpGet请求
   HttpCurl::getCurl(string $url,array $params=[],array $headers=[]);返回结果
   
   #####httpPost请求
   HttpCurl::postCurl($url,$params=[],$headers=[],$asJson=true);返回结果
   
   #####推送mq信息
   MqService::pushMessage($message,$queueName);
   
   #####企微报警
   QiWeiTool::sendMessageToBaoJing("报警信息");
   