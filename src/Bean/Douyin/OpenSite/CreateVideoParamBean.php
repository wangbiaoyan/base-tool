<?php

namespace Qingrong\Tool\Bean\Douyin\OpenSite;

use Qingrong\Tool\Bean\ToolBaseBean;


class CreateVideoParamBean extends ToolBaseBean
{
    /**
     * 授权token
     * @var string $accessToken
     */
    private $accessToken = "";

    /**
     * openid
     * @var string $openId
     */
    private $openId = "";

    /**
     * 必须上传加密的video_id。加密的video_id通过调用/video/upload接口可以得到。
     * @var string $videoId
     */
    private $videoId = "";

    /**
     * 视频标题。可以带话题，@用户。注意：话题审核依旧遵循抖音的审核逻辑，强烈建议第三方谨慎拟定话题名称，避免强导流行为。
     * @var string $text
     */
    private $text = "";

    /**
     * 如果需要at其他用户。将text中@nickname对应的open_id放到这里
     * @var array $atUsers
     */
    private $atUsers = [];

    /**
     * 开发者在小程序中生成该页面时写的path地址
     * @var string $microAppUrl
     */
    private $microAppUrl = "";

    /**
     * 小程序id
     * @var string $microAppId
     */
    private $microAppId = "";

    /**
     * 地理位置id，poi_id可通过"查询视频携带的地点信息"能力获取
     * @var string $poiId
     */
    private $poiId = "";

    /**
     * 将传入的指定时间点对应帧设置为视频封面（单位：秒）
     * @var float $coverTsp
     */
    private $coverTsp = 0.0;

    /**
     * 为ture时，如果用户拥有门店推广能力，则用户发布视频所添加的地理位置默认开启门店推广
     * @var bool $poiCommerce
     */
    private $poiCommerce = false;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     */
    public function setOpenId(string $openId)
    {
        $this->openId = $openId;
    }

    /**
     * @return string
     */
    public function getVideoId(): string
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId(string $videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return array
     */
    public function getAtUsers(): array
    {
        return $this->atUsers;
    }

    /**
     * @param array $atUsers
     */
    public function setAtUsers(array $atUsers)
    {
        $this->atUsers = $atUsers;
    }

    /**
     * @return string
     */
    public function getMicroAppUrl(): string
    {
        return $this->microAppUrl;
    }

    /**
     * @param string $microAppUrl
     */
    public function setMicroAppUrl(string $microAppUrl)
    {
        $this->microAppUrl = $microAppUrl;
    }

    /**
     * @return string
     */
    public function getMicroAppId(): string
    {
        return $this->microAppId;
    }

    /**
     * @param string $microAppId
     */
    public function setMicroAppId(string $microAppId)
    {
        $this->microAppId = $microAppId;
    }

    /**
     * @return string
     */
    public function getPoiId(): string
    {
        return $this->poiId;
    }

    /**
     * @param string $poiId
     */
    public function setPoiId(string $poiId)
    {
        $this->poiId = $poiId;
    }

    /**
     * @return float
     */
    public function getCoverTsp(): float
    {
        return $this->coverTsp;
    }

    /**
     * @param float $coverTsp
     */
    public function setCoverTsp(float $coverTsp)
    {
        $this->coverTsp = $coverTsp;
    }

    /**
     * @return bool
     */
    public function isPoiCommerce(): bool
    {
        return $this->poiCommerce;
    }

    /**
     * @param bool $poiCommerce
     */
    public function setPoiCommerce(bool $poiCommerce)
    {
        $this->poiCommerce = $poiCommerce;
    }
}