<?php

namespace Qingrong\Tool\Bean\MiniApplet;

use Qingrong\Tool\Bean\ToolBaseBean;

class MerchantWithdrawParamBean extends ToolBaseBean
{
    /**
     * 小程序第三方平台应用 id。服务商发起提现请求的必填
     * @var string $thirdpartyId
     */
    private $thirdpartyId = "";

    /**
     * 小程序的 app_id；除服务商为自己提现外，其他情况必填
     * @var string $appId
     */
    private $appId = "";

    /**
     * 进件完成返回的商户号
     * @var string $merchantUid
     */
    private $merchantUid = "";

    /**
     * 提现渠道枚举值:alipay: 支付宝 wx: 微信 hz: 抖音支付 yeepay: 易宝 yzt: 担保支付企业版聚合账户
     * @var string $channelType
     */
    private $channelType = "";

    /**
     * 提现金额；单位分
     * @var int $withdrawAmount
     */
    private $withdrawAmount = 0;

    /**
     * 外部单号（开发者侧）；唯一标识一笔提现请求
     * @var string $outOrderId
     */
    private $outOrderId = "";

    /**
     *  提现结果通知接口（开发者自己的https服务）；如果不传默认用支付设置中的回调地址
     * @var string $callback
     */
    private $callback = "";

    /**
     * 向开发者回调时，会原样存储在回调参数的extra字段中返回
     * @var string $cpExtra
     */
    private $cpExtra = "";

    /**
     * 抖音信息和光合信号标识:不传或传0或1 按抖音信息提现；传2按光合信号提现
     * @var int $merchantEntity
     */
    private $merchantEntity = 0;

    /**
     * @return string
     */
    public function getThirdpartyId(): string
    {
        return $this->thirdpartyId;
    }

    /**
     * @param string $thirdpartyId
     */
    public function setThirdpartyId(string $thirdpartyId)
    {
        $this->thirdpartyId = $thirdpartyId;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getMerchantUid(): string
    {
        return $this->merchantUid;
    }

    /**
     * @param string $merchantUid
     */
    public function setMerchantUid(string $merchantUid)
    {
        $this->merchantUid = $merchantUid;
    }

    /**
     * @return string
     */
    public function getChannelType(): string
    {
        return $this->channelType;
    }

    /**
     * @param string $channelType
     */
    public function setChannelType(string $channelType)
    {
        $this->channelType = $channelType;
    }

    /**
     * @return int
     */
    public function getWithdrawAmount(): int
    {
        return $this->withdrawAmount;
    }

    /**
     * @param int $withdrawAmount
     */
    public function setWithdrawAmount(int $withdrawAmount)
    {
        $this->withdrawAmount = $withdrawAmount;
    }

    /**
     * @return string
     */
    public function getOutOrderId(): string
    {
        return $this->outOrderId;
    }

    /**
     * @param string $outOrderId
     */
    public function setOutOrderId(string $outOrderId)
    {
        $this->outOrderId = $outOrderId;
    }

    /**
     * @return string
     */
    public function getCallback(): string
    {
        return $this->callback;
    }

    /**
     * @param string $callback
     */
    public function setCallback(string $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @return string
     */
    public function getCpExtra(): string
    {
        return $this->cpExtra;
    }

    /**
     * @param string $cpExtra
     */
    public function setCpExtra(string $cpExtra)
    {
        $this->cpExtra = $cpExtra;
    }

    /**
     * @return int
     */
    public function getMerchantEntity(): int
    {
        return $this->merchantEntity;
    }

    /**
     * @param int $merchantEntity
     */
    public function setMerchantEntity(int $merchantEntity)
    {
        $this->merchantEntity = $merchantEntity;
    }
}