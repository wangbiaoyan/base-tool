<?php

namespace Qingrong\Tool\Bean\MiniApplet;

use Qingrong\Tool\Bean\ToolBaseBean;

class QueryMerchantBalanceParamBean extends ToolBaseBean
{
    /**
     * 小程序第三方平台应用 id。在服务商发起提现请求的条件下必填
     * @var string $thirdpartyId
     */
    private $thirdpartyId = "";

    /**
     * 小程序的 app_id。在服务商为自己提现的情况下可不填，其他情况必填
     * @var string $app_id
     */
    private $appId = "";

    /**
     * 进件完成返回的商户号
     * @var string $merchantUid
     */
    private $merchantUid = "";

    /**
     * 提现渠道枚举值:alipay: 支付宝 wx: 微信 hz: 抖音支付 yzt: 担保支付企业版聚合账户
     * @var string $channelType
     */
    private $channelType = "";

    /**
     * 抖音信息和光合信号主体标识:不传或传0或1 查抖音信息余额；传2查光合信号余额
     * @var int $merchantEntity
     */
    private $merchantEntity = 0;

    /**
     * @return string
     */
    public function getThirdpartyId(): string
    {
        return $this->thirdpartyId;
    }

    /**
     * @param string $thirdpartyId
     */
    public function setThirdpartyId(string $thirdpartyId)
    {
        $this->thirdpartyId = $thirdpartyId;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getMerchantUid(): string
    {
        return $this->merchantUid;
    }

    /**
     * @param string $merchantUid
     */
    public function setMerchantUid(string $merchantUid)
    {
        $this->merchantUid = $merchantUid;
    }

    /**
     * @return string
     */
    public function getChannelType(): string
    {
        return $this->channelType;
    }

    /**
     * @param string $channelType
     */
    public function setChannelType(string $channelType)
    {
        $this->channelType = $channelType;
    }

    /**
     * @return int
     */
    public function getMerchantEntity(): int
    {
        return $this->merchantEntity;
    }

    /**
     * @param int $merchantEntity
     */
    public function setMerchantEntity(int $merchantEntity)
    {
        $this->merchantEntity = $merchantEntity;
    }
}