<?php

namespace Qingrong\Tool\Bean\MiniApplet;

use Qingrong\Tool\Bean\ToolBaseBean;

class QueryWithdrawOrderParamBean extends ToolBaseBean
{
    /**
     * 小程序第三方平台应用 id。服务商发起提现请求的必填
     * @var string $thirdpartyId
     */
    private $thirdpartyId = "";

    /**
     * 小程序的 app_id；除服务商为自己提现外，其他情况必填
     * @var string $appId
     */
    private $appId = "";

    /**
     * 进件完成返回的商户号
     * @var string $merchantUid
     */
    private $merchantUid = "";

    /**
     * 提现渠道枚举值:alipay: 支付宝 wx: 微信 hz: 抖音支付 yeepay: 易宝 yzt: 担保支付企业版聚合账户
     * @var string $channelType
     */
    private $channelType = "";

    /**
     * 外部单号（开发者侧）；唯一标识一笔提现请求
     * @var string $outOrderId
     */
    private $outOrderId = "";

    /**
     * @return string
     */
    public function getThirdpartyId(): string
    {
        return $this->thirdpartyId;
    }

    /**
     * @param string $thirdpartyId
     */
    public function setThirdpartyId(string $thirdpartyId)
    {
        $this->thirdpartyId = $thirdpartyId;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId
     */
    public function setAppId(string $appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getMerchantUid(): string
    {
        return $this->merchantUid;
    }

    /**
     * @param string $merchantUid
     */
    public function setMerchantUid(string $merchantUid)
    {
        $this->merchantUid = $merchantUid;
    }

    /**
     * @return string
     */
    public function getChannelType(): string
    {
        return $this->channelType;
    }

    /**
     * @param string $channelType
     */
    public function setChannelType(string $channelType)
    {
        $this->channelType = $channelType;
    }

    /**
     * @return string
     */
    public function getOutOrderId(): string
    {
        return $this->outOrderId;
    }

    /**
     * @param string $outOrderId
     */
    public function setOutOrderId(string $outOrderId)
    {
        $this->outOrderId = $outOrderId;
    }
}