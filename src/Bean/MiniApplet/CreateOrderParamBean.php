<?php


namespace Qingrong\Tool\Bean\MiniApplet;


use Qingrong\Tool\Bean\ToolBaseBean;

class CreateOrderParamBean extends ToolBaseBean
{
    /**
     * 商户订单编号
     */
    private $outOrderNo;

    /**
     * 支付金额单位(分)
     */
    private $totalAmount;

    /**
     * 商品描述
     */
    private $subject;

    /**
     * 商品详情
     */
    private $body;

    /**
     * 订单过期时间(订单过期时间(秒)。最小5分钟，最大2天，小于5分钟会被置为5分钟，大于2天会被置为2天)
     */
    private $validTime;

    /**
     * 支付回调地址
     */
    private $notifyUrl;

    /**
     * 开发者自定义字段，回调原样回传。 超过最大长度会被截断
     */
    private $cpExtra;

    /**
     * 支付场景(指定支付场景。（1）不填-默认为普通担保支付；（2）LinkPay-指定担保支付接入H5版本)
     */
    private $payScene;

    /**
     * 用户的openId
     * @var string $openId
     */
    private $openId;

    /**
     * 商户号
     * @var string $mchid
     */
    private $mchid;

    /**
     * ip
     * @var string $ip
     */
    private $ip;

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getMchid()
    {
        return $this->mchid;
    }

    /**
     * @param string $mchid
     */
    public function setMchid(string $mchid)
    {
        $this->mchid = $mchid;
    }

    /**
     * @return string
     */
    public function getOpenId()
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     */
    public function setOpenId($openId)
    {
        $this->openId = $openId;
    }

    /**
     * @return mixed
     */
    public function getPayScene()
    {
        return $this->payScene;
    }

    /**
     * @param mixed $payScene
     */
    public function setPayScene($payScene)
    {
        $this->payScene = $payScene;
    }

    /**
     * @return mixed
     */
    public function getCpExtra()
    {
        return $this->cpExtra;
    }

    /**
     * @param mixed $cpExtra
     */
    public function setCpExtra($cpExtra)
    {
        $this->cpExtra = $cpExtra;
    }

    /**
     * @return mixed
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * @param mixed $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return mixed
     */
    public function getOutOrderNo()
    {
        return $this->outOrderNo;
    }

    /**
     * @param mixed $outOrderNo
     */
    public function setOutOrderNo($outOrderNo)
    {
        $this->outOrderNo = $outOrderNo;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getValidTime()
    {
        return $this->validTime;
    }

    /**
     * @param mixed $validTime
     */
    public function setValidTime($validTime)
    {
        $this->validTime = $validTime;
    }
}
