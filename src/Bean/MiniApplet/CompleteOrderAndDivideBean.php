<?php

namespace Qingrong\Tool\Bean\MiniApplet;

use Qingrong\Tool\Bean\ToolBaseBean;

/**
 * 结算并分账参数
 */
class CompleteOrderAndDivideBean extends ToolBaseBean
{
    /**
     * 开发者侧的结算单号，相同结算单号小程序平台会进行幂等处理。 只能使用数字、大小写字母_-*。
     * @var string $outSettleNo
     */
    private $outSettleNo="";

    /**
     * 该笔分账单关联的商户订单单号，标识进行结算的订单（即对应支付预下单接口的"开发者侧的订单号out_order_no"参数）
     * @var string $outOrderNo
     */
    private $outOrderNo="";

    /**
     * 结算描述，长度限制 80 个字符
     * @var string $settleDesc
     */
    private $settleDesc="";

    /**
     * 其他分账方信息，分账分配参数 SettleParameter 数组序列化后生成的 json 格式字符串
     * @var string $settleParams
     */
    private $settleParams="";

    /**
     * 开发者自定义字段，回调原样回传
     * @var string $cpExtra
     */
    private $cpExtra="";

    /**
     * 商户自定义分账回调地址（要求https）
     * @var string $notifyUrl
     */
    private $notifyUrl="";

    /**
     * 第三方平台服务商 id，非服务商模式留空
     * @var string $thirdpartyId
     */
    private $thirdpartyId="";

    /**
     * 特别注意：该参数类型为string而非bool。 1. 如果为'true'（默认值为'true'，即不传该字段等价于'true'），则该笔订单剩余未分账金额会一并结算给商户； 2. 如果为'false'，该笔订单剩余未分账的金额不会一并结算给商户，可以对该笔订单再次进行分账；
     * @var string $finish
     */
    private $finish="";

    /**
     * @return string
     */
    public function getOutSettleNo(): string
    {
        return $this->outSettleNo;
    }

    /**
     * @param string $outSettleNo
     */
    public function setOutSettleNo(string $outSettleNo)
    {
        $this->outSettleNo = $outSettleNo;
    }

    /**
     * @return string
     */
    public function getOutOrderNo(): string
    {
        return $this->outOrderNo;
    }

    /**
     * @param string $outOrderNo
     */
    public function setOutOrderNo(string $outOrderNo)
    {
        $this->outOrderNo = $outOrderNo;
    }

    /**
     * @return string
     */
    public function getSettleDesc(): string
    {
        return $this->settleDesc;
    }

    /**
     * @param string $settleDesc
     */
    public function setSettleDesc(string $settleDesc)
    {
        $this->settleDesc = $settleDesc;
    }

    /**
     * @return string
     */
    public function getSettleParams(): string
    {
        return $this->settleParams;
    }

    /**
     * @param string $settleParams
     */
    public function setSettleParams(string $settleParams)
    {
        $this->settleParams = $settleParams;
    }

    /**
     * @return string
     */
    public function getCpExtra(): string
    {
        return $this->cpExtra;
    }

    /**
     * @param string $cpExtra
     */
    public function setCpExtra(string $cpExtra)
    {
        $this->cpExtra = $cpExtra;
    }

    /**
     * @return string
     */
    public function getNotifyUrl(): string
    {
        return $this->notifyUrl;
    }

    /**
     * @param string $notifyUrl
     */
    public function setNotifyUrl(string $notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return string
     */
    public function getThirdpartyId(): string
    {
        return $this->thirdpartyId;
    }

    /**
     * @param string $thirdpartyId
     */
    public function setThirdpartyId(string $thirdpartyId)
    {
        $this->thirdpartyId = $thirdpartyId;
    }

    /**
     * @return string
     */
    public function getFinish(): string
    {
        return $this->finish;
    }

    /**
     * @param string $finish
     */
    public function setFinish(string $finish)
    {
        $this->finish = $finish;
    }
}