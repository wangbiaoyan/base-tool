<?php

namespace Qingrong\Tool\Bean\MiniApplet;

use Qingrong\Tool\Bean\ToolBaseBean;

class SendImMessageParamBean extends ToolBaseBean
{
    /**
     * 会话ID
     */
    private $conversionId;

    /**
     * 支付url
     */
    private $sendUrl;

    /**
     * 用户的 openid
     */
    private $toUserId;

    /**
     * 1-发送支付链接 2-发送支付成功
     */
    private $msgType;

    /**
     * 客服号 openid
     */
    private $openId;

    /**
     * accessToken
     */
    private $accessToken;

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getOpenId()
    {
        return $this->openId;
    }

    /**
     * @param mixed $openId
     */
    public function setOpenId($openId)
    {
        $this->openId = $openId;
    }

    /**
     * @return mixed
     */
    public function getConversionId()
    {
        return $this->conversionId;
    }

    /**
     * @param mixed $conversionId
     */
    public function setConversionId($conversionId)
    {
        $this->conversionId = $conversionId;
    }

    /**
     * @return mixed
     */
    public function getSendUrl()
    {
        return $this->sendUrl;
    }

    /**
     * @param mixed $sendUrl
     */
    public function setSendUrl($sendUrl)
    {
        $this->sendUrl = $sendUrl;
    }

    /**
     * @return mixed
     */
    public function getToUserId()
    {
        return $this->toUserId;
    }

    /**
     * @param mixed $toUserId
     */
    public function setToUserId($toUserId)
    {
        $this->toUserId = $toUserId;
    }

    /**
     * @return mixed
     */
    public function getMsgType()
    {
        return $this->msgType;
    }

    /**
     * @param mixed $msgType
     */
    public function setMsgType($msgType)
    {
        $this->msgType = $msgType;
    }
}