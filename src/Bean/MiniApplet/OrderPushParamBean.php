<?php


namespace Qingrong\Tool\Bean\MiniApplet;


use Qingrong\Tool\Bean\ToolBaseBean;

/**
 * 订单推送参数
 * Class OrderPushParamBean
 * @package Qingrong\Tool\Bean\MiniApplet
 */
class OrderPushParamBean extends ToolBaseBean
{
    /**
     * token
     * @var
     */
    private $accessToken;

    /**
     * openId
     * @var string $openId
     */
    private $openId;

    /**
     * 订单明细
     * @var string $orderDetail
     */
    private $orderDetail;

    /**
     * 订单状态 0：待支付 1：已支付 2：已取消（用户主动取消或者超时未支付导致的关单） 4：已核销（核销状态是整单核销,即一笔订单买了 3 个券，核销是指 3 个券核销的整单） 5：退款中 6：已退款 8：退款失败
     * @var int $orderStatus
     */
    private $orderStatus;

    /**
     * 订单类型，枚举值: 0：普通小程序订单（非POI订单） 9101：团购券订单（POI 订单） 9001：景区门票订单（POI订单）
     * @var int $orderType
     */
    private $orderType;

    /**
     * 订单信息变更时间，13 位毫秒级时间戳
     * @var int $updateTime
     */
    private $updateTime;

    /**
     * 自定义字段，用于关联具体业务场景下的特殊参数，长度 < 2048byte
     * @var string $extra
     */
    private $extra;

    /**
     * app名称
     * @var string $appName
     */
    private $appName;

    /**
     * @return string
     */
    public function getAppName(): string
    {
        return $this->appName;
    }

    /**
     * @param string $appName
     */
    public function setAppName(string $appName)
    {
        $this->appName = $appName;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getOpenId(): string
    {
        return $this->openId;
    }

    /**
     * @param string $openId
     */
    public function setOpenId(string $openId)
    {
        $this->openId = $openId;
    }

    /**
     * @return string
     */
    public function getOrderDetail(): string
    {
        return $this->orderDetail;
    }

    /**
     * @param string $orderDetail
     */
    public function setOrderDetail(string $orderDetail)
    {
        $this->orderDetail = $orderDetail;
    }

    /**
     * @return int
     */
    public function getOrderStatus(): int
    {
        return $this->orderStatus;
    }

    /**
     * @param int $orderStatus
     */
    public function setOrderStatus(int $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return int
     */
    public function getOrderType(): int
    {
        return $this->orderType;
    }

    /**
     * @param int $orderType
     */
    public function setOrderType(int $orderType)
    {
        $this->orderType = $orderType;
    }

    /**
     * @return int
     */
    public function getUpdateTime(): int
    {
        return $this->updateTime;
    }

    /**
     * @param int $updateTime
     */
    public function setUpdateTime(int $updateTime)
    {
        $this->updateTime = $updateTime;
    }

    /**
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param string $extra
     */
    public function setExtra(string $extra)
    {
        $this->extra = $extra;
    }
}
