<?php


namespace Qingrong\Tool\Bean\MiniApplet;


use Qingrong\Tool\Bean\ToolBaseBean;

class RefundOrderParamBean extends ToolBaseBean
{
    /**
     * 商户分配支付单号，标识进行退款的订单
     */
    private $outOrderNo;

    /**
     * 商户分配退款号，保证在商户中唯一
     */
    private $outRefundNo;

    /**
     * 退款原因
     */
    private $reason;

    /**
     * 退款金额，单位分
     */
    private $refundAmount;

    /**
     * 开发者自定义字段，回调原样回传
     */
    private $cpExtra;

    /**
     * 商户自定义回调地址
     */
    private $notifyUrl;

    /**
     * @return mixed
     */
    public function getCpExtra()
    {
        return $this->cpExtra;
    }

    /**
     * @param mixed $cpExtra
     */
    public function setCpExtra($cpExtra)
    {
        $this->cpExtra = $cpExtra;
    }

    /**
     * @return mixed
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * @param mixed $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * @return mixed
     */
    public function getOutOrderNo()
    {
        return $this->outOrderNo;
    }

    /**
     * @param mixed $outOrderNo
     */
    public function setOutOrderNo($outOrderNo)
    {
        $this->outOrderNo = $outOrderNo;
    }

    /**
     * @return mixed
     */
    public function getOutRefundNo()
    {
        return $this->outRefundNo;
    }

    /**
     * @param mixed $outRefundNo
     */
    public function setOutRefundNo($outRefundNo)
    {
        $this->outRefundNo = $outRefundNo;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getRefundAmount()
    {
        return $this->refundAmount;
    }

    /**
     * @param mixed $refundAmount
     */
    public function setRefundAmount($refundAmount)
    {
        $this->refundAmount = $refundAmount;
    }
}
