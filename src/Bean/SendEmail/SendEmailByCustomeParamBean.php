<?php


namespace Qingrong\Tool\Bean\SendEmail;


use Qingrong\Tool\Bean\ToolBaseBean;

class SendEmailByCustomeParamBean extends ToolBaseBean
{
    /**
     * 邮件标题
     * @var string $title
     */
    private $title;

    /**
     * 邮件名称
     * @var $name string
     */
    private $name;

    /**
     * 接收的邮件地址
     * @var string $email
     */
    private $email;

    /**
     * 邮件内容
     * @var string $content
     */
    private $content;

    /**
     * 邮件服务器账号
     * @var string $serviceName
     */
    private $serviceName;

    /**
     * 邮件服务器密码
     * @var string $servicePassword
     */
    private $servicePassword;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     */
    public function setServiceName(string $serviceName)
    {
        $this->serviceName = $serviceName;
    }

    /**
     * @return string
     */
    public function getServicePassword(): string
    {
        return $this->servicePassword;
    }

    /**
     * @param string $servicePassword
     */
    public function setServicePassword(string $servicePassword)
    {
        $this->servicePassword = $servicePassword;
    }
}
