<?php

namespace Qingrong\Tool\Bean\Advertisement\DouYinXingTuAdcertisement;

use Qingrong\Tool\Bean\ToolBaseBean;

class WechatMiniAppletUploadParamBean extends ToolBaseBean
{
    /**
     * 点击下发的clickId
     */
    private $clickId;

    /**
     * 事件类型
     * @var int $eventType
     */
    private $eventType;

    /**
     * 额外回传数据
     * @var array $props
     */
    private $props;

    /**
     * @return mixed
     */
    public function getClickId()
    {
        return $this->clickId;
    }

    /**
     * @param mixed $clickId
     */
    public function setClickId($clickId)
    {
        $this->clickId = $clickId;
    }

    /**
     * @return int
     */
    public function getEventType(): int
    {
        return $this->eventType;
    }

    /**
     * @param int $eventType
     */
    public function setEventType(int $eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return array
     */
    public function getProps(): array
    {
        return $this->props;
    }

    /**
     * @param array $props
     */
    public function setProps(array $props)
    {
        $this->props = $props;
    }
}