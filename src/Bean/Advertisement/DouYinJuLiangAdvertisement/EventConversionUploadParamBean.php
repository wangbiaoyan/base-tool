<?php

namespace Qingrong\Tool\Bean\Advertisement\DouYinJuLiangAdvertisement;

use Qingrong\Tool\Bean\ToolBaseBean;

class EventConversionUploadParamBean extends ToolBaseBean
{
    /**
     * 回传的事件
     * @var string $eventType
     */
    private $eventType;

    /**
     * 上下文信息
     * @var array $context
     */
    private $context;

    /**
     * 毫秒级时间戳
     * @var $timestamp
     */
    private $timestamp;

    /**
     * 权重值
     * @var float $eventWeight
     */
    private $eventWeight;

    /**
     * 附加属性
     * @var $properties
     */
    private $properties;

    /**
     * 上报唯一ID
     * @var $outerEventId
     */
    private $outerEventId;

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }

    /**
     * @return mixed
     */
    public function getOuterEventId()
    {
        return $this->outerEventId;
    }

    /**
     * @param mixed $outerEventId
     */
    public function setOuterEventId($outerEventId)
    {
        $this->outerEventId = $outerEventId;
    }

    /**
     * @return float
     */
    public function getEventWeight()
    {
        return $this->eventWeight;
    }

    /**
     * @param float $eventWeight
     */
    public function setEventWeight(float $eventWeight)
    {
        $this->eventWeight = $eventWeight;
    }

    /**
     * @return string
     */
    public function getEventType(): string
    {
        return $this->eventType;
    }

    /**
     * @param string $eventType
     */
    public function setEventType(string $eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @param array $context
     */
    public function setContext(array $context)
    {
        $this->context = $context;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }
}