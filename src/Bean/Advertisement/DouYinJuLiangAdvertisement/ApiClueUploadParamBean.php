<?php

namespace Qingrong\Tool\Bean\Advertisement\DouYinJuLiangAdvertisement;

use Qingrong\Tool\Bean\ToolBaseBean;

class ApiClueUploadParamBean extends ToolBaseBean
{
    /**
     * link字段就是广告被打开的落地页的原始真实 url，广告主需要把这个 url ，encode 之后传递给巨量引擎。
     * @var string $link
     */
    private $link;

    /**
     * 事件类型 具体参考 JuLiangAdvertisementEnum枚举类型
     * @var int $eventType
     */
    private $eventType;

    /**
     * UTC 时间戳，单位：秒
     * @var int $convTime
     */
    private $convTime;

    /**
     * 数据来源，比如来自 talkingdata的激活回调, 可以填写 TD
     * @var string $source
     */
    private $source;

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getEventType(): int
    {
        return $this->eventType;
    }

    /**
     * @param int $eventType
     */
    public function setEventType(int $eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return int
     */
    public function getConvTime(): int
    {
        return $this->convTime;
    }

    /**
     * @param int $convTime
     */
    public function setConvTime(int $convTime)
    {
        $this->convTime = $convTime;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source)
    {
        $this->source = $source;
    }
}