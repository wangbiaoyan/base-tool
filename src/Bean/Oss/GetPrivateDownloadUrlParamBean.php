<?php


namespace Qingrong\Tool\Bean\Oss;


use Qingrong\Tool\Bean\ToolBaseBean;

class GetPrivateDownloadUrlParamBean extends ToolBaseBean
{
    /**
     * 地址
     * @var $domain string
     */
    private $domain;

    /**
     * 文件地址
     * @var $path string
     */
    private $path;

    /**
     * 过期时间
     * @var $expireTime int
     */
    private $expireTime;

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return int
     */
    public function getExpireTime(): int
    {
        return $this->expireTime;
    }

    /**
     * @param int $expireTime
     */
    public function setExpireTime(int $expireTime)
    {
        $this->expireTime = $expireTime;
    }
}
