<?php


namespace Qingrong\Tool\Bean\Mq;


use Qingrong\Tool\Bean\ToolBaseBean;
use Qingrong\Tool\Mq\MqConsumeInterface;

/**
 * mq消费参数
 * Class ConsumeParamBean
 * @package Qingrong\Tool\Bean\Mq
 */
class ConsumeParamBean extends ToolBaseBean
{
    /**
     * 交换机名称
     * @var string $exchangeName
     */
    private $exchangeName;

    /**
     * 交换机类型
     * @var string $exchangeType
     */
    private $exchangeType;

    /**
     * 队列名称
     * @var string $queueName
     */
    private $queueName;

    /**
     * 路由键
     * @var string $routeKey
     */
    private $routeKey;

    /**
     * 消费函数
     * @var array $callback
     */
    private $callback;

    /**
     * @var $object MqConsumeInterface
     */
    private $object;

    /**
     * @return MqConsumeInterface
     */
    public function getObject(): MqConsumeInterface
    {
        return $this->object;
    }

    /**
     * @param MqConsumeInterface $object
     */
    public function setObject(MqConsumeInterface $object)
    {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getExchangeName(): string
    {
        return $this->exchangeName;
    }

    /**
     * @param string $exchangeName
     */
    public function setExchangeName(string $exchangeName)
    {
        $this->exchangeName = $exchangeName;
    }

    /**
     * @return string
     */
    public function getExchangeType(): string
    {
        return $this->exchangeType;
    }

    /**
     * @param string $exchangeType
     */
    public function setExchangeType(string $exchangeType)
    {
        $this->exchangeType = $exchangeType;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName;
    }

    /**
     * @param string $queueName
     */
    public function setQueueName(string $queueName)
    {
        $this->queueName = $queueName;
    }

    /**
     * @return string
     */
    public function getRouteKey(): string
    {
        return $this->routeKey;
    }

    /**
     * @param string $routeKey
     */
    public function setRouteKey(string $routeKey)
    {
        $this->routeKey = $routeKey;
    }

    /**
     * @return array
     */
    public function getCallback(): array
    {
        return $this->callback;
    }

    /**
     * @param array $callback
     */
    public function setCallback(array $callback)
    {
        $this->callback = $callback;
    }
}
