<?php


namespace Qingrong\Tool\Bean\Qr;


use Qingrong\Tool\Bean\ToolBaseBean;

class CreateQrLogoImgServiceParamBean extends ToolBaseBean
{
    /**
     * 文件名称
     * @var string $fileName
     */
    private $fileName;

    /**
     * 二维码内容
     * @var string $qrContent
     */
    private $qrContent;

    /**
     * 二维码大小
     * @var int $qrSize
     */
    private $qrSize;

    /**
     * logo地址
     * @var string $logoName
     */
    private $logoName;

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getQrContent(): string
    {
        return $this->qrContent;
    }

    /**
     * @param string $qrContent
     */
    public function setQrContent(string $qrContent)
    {
        $this->qrContent = $qrContent;
    }

    /**
     * @return int
     */
    public function getQrSize(): int
    {
        return $this->qrSize;
    }

    /**
     * @param int $qrSize
     */
    public function setQrSize(int $qrSize)
    {
        $this->qrSize = $qrSize;
    }

    /**
     * @return string
     */
    public function getLogoName(): string
    {
        return $this->logoName;
    }

    /**
     * @param string $logoName
     */
    public function setLogoName(string $logoName)
    {
        $this->logoName = $logoName;
    }
}
