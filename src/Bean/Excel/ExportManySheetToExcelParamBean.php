<?php


namespace Qingrong\Tool\Bean\Excel;


use Qingrong\Tool\Bean\ToolBaseBean;

class ExportManySheetToExcelParamBean extends ToolBaseBean
{
    /**
     * sheet头数据
     * @var array $sheetHeaderData
     */
    private $sheetHeaderData;

    /**
     * sheet数据
     * @var array $sheetDatas
     */
    private $sheetDatas;

    /**
     * 文件名称
     * @var string $fileName
     */
    private $fileName;

    /**
     * sheet名称
     * @var array $sheetNames
     */
    private $sheetNames;

    /**
     * @return array
     */
    public function getSheetNames(): array
    {
        return $this->sheetNames;
    }

    /**
     * @param array $sheetNames
     */
    public function setSheetNames(array $sheetNames)
    {
        $this->sheetNames = $sheetNames;
    }

    /**
     * @return array
     */
    public function getSheetHeaderData(): array
    {
        return $this->sheetHeaderData;
    }

    /**
     * @param array $sheetHeaderData
     */
    public function setSheetHeaderData(array $sheetHeaderData)
    {
        $this->sheetHeaderData = $sheetHeaderData;
    }

    /**
     * @return array
     */
    public function getSheetDatas(): array
    {
        return $this->sheetDatas;
    }

    /**
     * @param array $sheetDatas
     */
    public function setSheetDatas(array $sheetDatas)
    {
        $this->sheetDatas = $sheetDatas;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
    }
}
