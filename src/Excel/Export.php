<?php


namespace Qingrong\Tool\Excel;


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Qingrong\Tool\Bean\Excel\ExportManySheetToExcelParamBean;
use Qingrong\Tool\Exception\ExcelException;

class Export
{
    /**
     * params $headerList 头部列表信息(一维数组) 必传
     * params $data 导出的数据(二维数组)  必传
     * params $filename 文件名称转码 必传
     * PS:出现数字格式化情况，可添加看不见的符号，使其正常，如:"\t"
     **/
    public function exportToCsv($headerList = [] , $data = [] , $fileName = ''){
        //文件名称转码
//        $fileName = iconv('UTF-8', 'GBK', $fileName);
        //设置header头
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $fileName . '.csv');
        header('Cache-Control: max-age=0');
        //打开PHP文件句柄,php://output,表示直接输出到浏览器
        $fp = fopen("php://output","a");
        fwrite($fp, chr(0xEF).chr(0xBB).chr(0xBF)); // 添加 BOM
        //输出Excel列表名称信息
        foreach ($headerList as $key => $value) {
//            $headerList[$key] = iconv('UTF-8', 'GBK', $value);//CSV的EXCEL支持BGK编码，一定要转换，否则乱码
            $headerList[$key] = $value;//CSV的EXCEL支持BGK编码，一定要转换，否则乱码
        }
        //使用fputcsv将数据写入文件句柄
        fputcsv($fp, $headerList);
        //计数器
        $num = 0;
        //每隔$limit行，刷新一下输出buffer,不要太大亦不要太小
        $limit = 100000;
        //逐行去除数据,不浪费内存
        $count = count($data);
        for($i = 0 ; $i < $count ; $i++){
            $num++;
            //刷新一下输出buffer，防止由于数据过多造成问题
            if($limit == $num){
                ob_flush();
                flush();
                $num = 0;
            }
            $row = $data[$i];
            foreach ($row as $key => $value) {
                $value = is_numeric($value) ? $value."\t" : $value;//解决输出长度较长的数字型变成科学计数法
                $row[$key] = $value;
//                $row[$key] = iconv('UTF-8', 'GBK', $value);
            }
            fputcsv($fp, $row);
        }
    }

    public function exportToExcel($headerList = [] , $datas = [] , $fileName = '')
    {
        $filename = $fileName.'.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($fileName);
        foreach ($headerList as $key=>$header){
            //设置标题
            $worksheet->setCellValueByColumnAndRow((int)$key+1, 1, $header);
        }
        foreach ($datas as $kk=>$data){
            $tmp = 0;
            foreach ($data as $k=>$val){
                //设置数据
                $worksheet->setCellValueByColumnAndRow((int)$tmp+1,(int)$kk+2,$val);
                $tmp++;
            }
        }
        $writer =IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function importExcel($file)
    {
        $reader = IOFactory::createReader('Xlsx');
        //打开文件、载入excel表格
        $spreadsheet = $reader->load($file);
        # 获取活动工作薄
        return $spreadsheet->getActiveSheet()->toArray();
    }

    /**
     * 导出多个sheet到excel
     * @param ExportManySheetToExcelParamBean $exportManySheetToExcelParamBean
     */
    public function exportManySheetToExcel(ExportManySheetToExcelParamBean $exportManySheetToExcelParamBean)
    {
        $fileName = $exportManySheetToExcelParamBean->getFileName().'.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$fileName.'"');
        header('Cache-Control: max-age=0');
        $sheetHeaderDatas = $exportManySheetToExcelParamBean->getSheetHeaderData();
        $datas = $exportManySheetToExcelParamBean->getSheetDatas();
        $sheetNames = $exportManySheetToExcelParamBean->getSheetNames();
        if (count($sheetHeaderDatas) != count($datas)){
            throw new ExcelException("sheet数量不正确");
        }

        $spreadsheet = new Spreadsheet();

        foreach ($sheetHeaderDatas as $xiabiao=>$sheetHeaderData){
            $worksheet = $spreadsheet->createSheet($xiabiao);
            $worksheet->setTitle($sheetNames[$xiabiao]);
            foreach ($sheetHeaderData as $key=>$header){
                //设置标题
                $worksheet->setCellValueByColumnAndRow((int)$key+1, 1, $header);
            }
            foreach ($datas[$xiabiao] as $kk=>$data){
                $tmp = 0;
                foreach ($data as $k=>$val){
                    //设置数据
                    $worksheet->setCellValueByColumnAndRow((int)$tmp+1,(int)$kk+2,$val);
                    $tmp++;
                }
            }

        }
        $writer =IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
