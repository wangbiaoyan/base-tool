<?php

namespace Qingrong\Tool\Feishu;

class FeiShu
{

    protected $base_ur = "https://open.feishu.cn/open-apis";
    protected $app_id;
    protected $app_secret;
    protected $headers;

    public function setAppId(string $app_id)
    {
        $this->app_id = $app_id;
        return $this;
    }

    public function setAppSecret(string $app_secret)
    {
        $this->app_secret = $app_secret;
        return $this;
    }

    public function setToken(string $token)
    {
        $heares[] = "Content-Type:application/json";
        $heares[] = "Authorization:Bearer " . $token;
        $this->headers = $heares;
        return $this;
    }

    public function sendMessage($open_id, $sendData, $type)
    {
        $title = $sendData['title'] ?? '';
        $data = $sendData['data'] ?? '';
        $content = [];
        foreach ($data as $msg) {
            $content[] = [
                [
                    "tag"  => "text",
                    "text" => "$msg"
                ]
            ];
        }
        $body = [
            "receive_id" => $open_id,
            "msg_type"   => "post",
            "content"    => json_encode([
                "zh_cn" => [
                    "title"   => $title,
                    "content" => $content
                ]
            ])
        ];
        $receive_id_type = [
            'open_id',
            'chat_id'
        ];
        $params = [
            'receive_id_type' => $receive_id_type[$type] ?? 'open_id'
        ];
        $url = $this->base_ur . '/im/v1/messages?' . http_build_query($params);
        try {

            $response = $this->requestUrl($url, 0, 1, json_encode($body), $this->headers);
            if ($response['code'] != 0) {
                $result = [ 'code' => 1, 'msg' => $response['msg'] ];
            } else {
                $result = [ 'code' => 0, 'msg' => 'ok' ];
            }
        } catch (\Exception $e) {
            $result = [ 'code' => 1, 'msg' => $e->getMessage() ];
        }
        return $result;
    }

    public function getChatid()
    {
        $url = $this->base_ur . '/im/v1/chats';
        try {
            $response = $this->requestUrl($url, 0, 0, [], $this->headers);
            if ($response['code'] != 0) {
                $result = [ 'code' => 1, 'data' => $response['msg'] ];
            } else {
                $result = [ 'code' => 0, 'data' => $response['data']['items'] ];
            }
        } catch (\Exception $e) {
            $result = [ 'code' => 1, 'data' => $e->getMessage() ];
        }
        return $result;
    }

    public function getOpenid($mobiles = [])
    {
        $url = $this->base_ur . '/contact/v3/users/batch_get_id';
        $body = [
            "mobiles" => $mobiles
        ];
        try {
            $response = $this->requestUrl($url, 0, 1, json_encode($body), $this->headers);
            if ($response['code'] != 0) {
                $result = [ 'code' => 1, 'data' => $response['msg'] ];
            } else {
                $result = [ 'code' => 0, 'data' => $response['data']['user_list'] ];
            }
        } catch (\Exception $e) {
            $result = [ 'code' => 1, 'data' => $e->getMessage() ];
        }
        return $result;
    }

    public function getToken()
    {
        try {
            $url = $this->base_ur . "/auth/v3/app_access_token/internal";
            $body = [
                "app_id"     => $this->app_id,
                "app_secret" => $this->app_secret
            ];
            $response = $this->requestUrl($url, 0, 1, $body);
            if ($response['code'] != 0) {
                $result = false;
            } else {
                $result = $response['tenant_access_token'];
            }
        } catch (\Exception $e) {
            $result = false;
        }
        return $result;
    }

    public function requestUrl($url, $flag = 0, $type = 0, $post_data = [], $headers = [], $ext_params = [], $tout = 0)
    {
        // 初始化一个 cURL 对象
        $curl = curl_init();
        // 设置你需要抓取的URL
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($tout) {
            //设置超时时间/秒
            curl_setopt($curl, CURLOPT_TIMEOUT, $tout);
        }
        // 设置header
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if ($type == 1) {       // post请求
            curl_setopt($curl, CURLOPT_POST, 1);
            $post_data = is_array($post_data) ? http_build_query($post_data) : $post_data;
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        }
        foreach ($ext_params as $key => $value) {
            curl_setopt($curl, $key, $value);
        }
        // 运行cURL，请求网页
        $data = curl_exec($curl);
        // 关闭URL请求
        curl_close($curl);
        if (!$flag) {
            $data = json_decode($data, true);
        }
        return $data;
    }

}
