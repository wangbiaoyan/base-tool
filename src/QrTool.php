<?php


namespace Qingrong\Tool;

use Qingrong\Tool\Bean\Qr\CreateQrLogoImgServiceParamBean;
use Qingrong\Tool\Qr\QrService;

/**
 * 二维码类
 * Class QrTool
 * @package Tool
 */
class QrTool
{
    /**
     * 获取二维码内容
     * @param $imgPath string 二维码地址
     */
    public static function getQrContent($imgPath)
    {
        return QrService::getQrContentService($imgPath);
    }

    /**
     * 创建二维码
     * @param $content string 二维码内容
     * @param $fileName string 文件名称
     * @return string
     */
    public static function creatrQrimg($content, $fileName)
    {
        return QrService::createQrImgService($content,$fileName);
    }

    /**
     * 创建带有logo的二维码
     * @param CreateQrLogoImgServiceParamBean $createQrLogoImgServiceParamBean
     * @return string|null
     */
    public static function createQrLogoImg(CreateQrLogoImgServiceParamBean $createQrLogoImgServiceParamBean)
    {
        return QrService::createQrLogoImgService($createQrLogoImgServiceParamBean);
    }
}
