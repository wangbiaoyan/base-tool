<?php


namespace Qingrong\Tool\Mq;


use Qingrong\Tool\Bean\Mq\ConsumeParamBean;
use Qingrong\Tool\Exception\MqException;
use Qingrong\Tool\MqTool;

abstract class MqConsumeInterface
{
    /**
     * 交换机名称
     * @var string $exchangeName
     */
    protected $exchangeName;

    /**
     * 队列名称
     * @var string $queueName
     */
    protected $queueName;

    /**
     * 路由键
     * @var string $routeKey
     */
    protected $routeKey;

    /**
     * 交换机类型(默认直连模式)
     * @var string $exchangeType
     */
    protected $exchangeType = AMQP_EX_TYPE_DIRECT;

    /**
     * 消费前置动作
     * @return mixed
     */
    public function bootStart()
    {
    }

    /**
     * 版本号
     * @var string $version
     */
    protected $version = "";

    public function getVersion()
    {
        return $this->version;
    }

    /**
     * 消费后置动作
     * @return mixed
     */
    public function bootEnd()
    {
    }

    /**
     * 异常处理
     * @return void
     */
    public function exceptionHandle(\Throwable $exption)
    {

    }

    /**
     * @param $AMQPEnvelope
     * @return void
     */
    public function sleepHandle($AMQPEnvelope)
    {

    }

    /**
     * 消费处理函数
     * @param \AMQPEnvelope $AMQPEnvelope
     * @param \AMQPQueue $AMQPQueue
     * @return mixed
     */
    public abstract function consumeCallback(\AMQPEnvelope $AMQPEnvelope, \AMQPQueue $AMQPQueue);

    public function handleConsume()
    {
        $params = [
            "exchangeName" => $this->exchangeName,
            "exchangeType" => $this->exchangeType,
            "queueName"    => $this->queueName,
            "routeKey"     => $this->routeKey,
            "callback"     => [ $this, "consumeCallback" ],
            "object"       => $this
        ];
        $consumeParamBean = new ConsumeParamBean($params);
        switch (config("queue.connections.default")) {
            case "rabbitmq":
                $rabbitMq = RabbitMq::getInstance();
                //声明交换机
                $rabbitMq->declareExchange($consumeParamBean->getExchangeName(), $consumeParamBean->getExchangeType());
                //声明队列
                $rabbitMq->declareQueue($consumeParamBean->getQueueName(), $consumeParamBean->getRouteKey());
                break;
            default:
                throw new MqException("未知消息队列");

        }

        while (true) {
            try {
                MqTool::consume($consumeParamBean, $rabbitMq);
            } catch (\Throwable $exception) {
                $this->exceptionHandle($exception);
                $rabbitMq->resetQueue($consumeParamBean->getQueueName(), $consumeParamBean->getRouteKey());
                sleep(2);
            }
        }
    }

    public function handleConsumeV1()
    {
        $params = [
            "exchangeName" => $this->exchangeName,
            "exchangeType" => $this->exchangeType,
            "queueName"    => $this->queueName,
            "routeKey"     => $this->routeKey,
            "callback"     => [ $this, "consumeCallback" ],
            "object"       => $this
        ];
        $consumeParamBean = new ConsumeParamBean($params);
        switch (config("queue.connections.default")) {
            case "rabbitmq":
                $rabbitMq = RabbitMq::getInstance();
                //声明交换机
                $rabbitMq->declareExchange($consumeParamBean->getExchangeName(), $consumeParamBean->getExchangeType());
                //声明队列
                $rabbitMq->declareQueue($consumeParamBean->getQueueName(), $consumeParamBean->getRouteKey());
                break;
            default:
                throw new MqException("未知消息队列");

        }

        while (true) {
            try {
                $rabbitMq->getQueue()->consume([ $this, "consumeCallback" ]);
            } catch (\Throwable $exception) {
                $this->exceptionHandle($exception);
            }
        }
    }
}
