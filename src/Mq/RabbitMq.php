<?php


namespace Qingrong\Tool\Mq;


use AMQPChannel;
use AMQPConnection;
use AMQPExchange;
use AMQPQueue;
use Qingrong\Tool\Exception\MqException;

class RabbitMq implements MqInterface
{
    /**
     * 配置数组
     * @var array $config
     */
    private $config;

    /**
     * 链接
     * @var AMQPConnection $connection
     */
    private $connection;

    /**
     * 渠道
     * @var AMQPChannel $channel
     */
    private $channel;

    /**
     * 交换机
     * @var AMQPExchange $exchange
     */
    private $exchange;

    /**
     * 消息队列
     * @var AMQPQueue $queue
     */
    private $queue;

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return AMQPConnection
     */
    public function getConnection(): AMQPConnection
    {
        return $this->connection;
    }

    /**
     * @param AMQPConnection $connection
     */
    public function setConnection(AMQPConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel(): AMQPChannel
    {
        return $this->channel;
    }

    /**
     * @param AMQPChannel $channel
     */
    public function setChannel(AMQPChannel $channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return AMQPExchange
     */
    public function getExchange(): AMQPExchange
    {
        return $this->exchange;
    }

    /**
     * @param AMQPExchange $exchange
     */
    public function setExchange(AMQPExchange $exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * @return AMQPQueue
     */
    public function getQueue(): AMQPQueue
    {
        return $this->queue;
    }

    /**
     * @param AMQPQueue $queue
     */
    public function setQueue(AMQPQueue $queue)
    {
        $this->queue = $queue;
    }


    protected static $instance = null;

    public static function getInstance($singleton = true)
    {
        if ($singleton) {
            if (is_null(self::$instance)) {
                self::$instance = new self();
            }
            return self::$instance;
        } else {
            return new self();
        }
    }

    public function __construct()
    {
        $this->connect();
    }

    /**
     * 链接主机
     * @return mixed
     */
    public function connect()
    {
        $connectConfig = config('queue.connections.rabbitmq');
        $this->config = [
            'host'      => $connectConfig['host'],
            'port'      => $connectConfig['port'],
            'login'     => $connectConfig['login'],
            'password'  => $connectConfig['password'],
            'vhost'     => $connectConfig['vhost'],
            'heartbeat' => 10
        ];

        $this->connection = new AMQPConnection($this->config);
        $this->connection->connect();
        $this->channel = new AMQPChannel($this->connection);
    }

    /**
     * 消费消息
     * @param callable $callback 处理函数
     * @return mixed
     */
    public function consume(callable $callback)
    {
        // TODO: Implement consume() method.
        $this->queue->consume($callback);
    }

    /**
     * 推送消息到mq
     * @param string $message 消息体
     * @param string $routingKey 路由键
     * @return mixed
     */
    public function publish(string $message, string $routingKey)
    {
        // TODO: Implement publish() method.
        $this->exchange->publish($message, $routingKey);
    }

    public function __destruct()
    {
        $this->connection->disconnect();
        $this->channel->close();
    }

    /**
     * 声明交换机
     */
    public function declareExchange($exchangeName, $exchangeType, $create = true)
    {
        $this->exchange = new AMQPExchange($this->channel);
        $this->exchange->setName($exchangeName);
        $this->exchange->setType($exchangeType);
        $this->exchange->setFlags(AMQP_DURABLE);
        if ($create) {
            $this->exchange->declareExchange();
        }
    }


    /**
     * 申明队列
     */
    public function declareQueue($queueName, $routeKey)
    {
        if (!$queueName) {
            throw new MqException('Queue name must be set');
        }

        $this->queue = new AMQPQueue($this->channel);
        $this->queue->setName($queueName);
        $this->queue->setFlags(AMQP_DURABLE);
        $this->queue->declareQueue();
        $this->queue->bind($this->exchange->getName(), $routeKey);
    }

    /**
     * 重置队列
     * @param $queueName
     * @param $routeKey
     * @return void
     * @throws MqException
     * @throws \AMQPConnectionException
     */
    public function resetQueue($queueName, $routeKey)
    {
        $this->channel = new AMQPChannel($this->connection);
        $this->declareQueue($queueName, $routeKey);
    }
}
