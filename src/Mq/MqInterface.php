<?php


namespace Qingrong\Tool\Mq;


interface MqInterface
{
    /**
     * 链接mq
     * @return mixed
     */
    public function connect();

    /**
     * 推送消息到mq
     * @param string $message 消息体
     * @param string $routingKey 路由键
     * @return mixed
     */
    public function publish(string $message,string $routingKey);

    /**
     * 消费消息
     * @param callable $callback 处理函数
     * @return mixed
     */
    public function consume(callable $callback);

}
