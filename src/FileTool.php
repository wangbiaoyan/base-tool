<?php


namespace Qingrong\Tool;


class FileTool
{
    /**
     * 创建指定的压缩包
     * @param $fileName string 压缩文件夹
     * @param $zipName string 压缩包名称
     */
    public static function createFileZip($fileName,string $zipName)
    {
        $rootPath = realpath($fileName);
        $zip = new \ZipArchive();
        $zip->open($zipName,\ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
    }
}
