<?php


namespace Qingrong\Tool;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Qingrong\Tool\Bean\SendEmail\SendEmailByCustomeParamBean;
use Qingrong\Tool\Exception\SendEmailException;

class SendEmail
{
    /**
     * 发送邮件
     * @param $title 邮件标题
     * @param $name 发送人名称
     * @param $email 收件人邮件账号
     * @param $content 邮件内容html格式代码
     */
    public static function sendEmail($title='',$name='',$email='',$content='')
    {
        if($title=='' || $name=='' || $email=='' || $content==''){
            return "缺少参数";
        }
        $mail = new PHPMailer(true);

        try {
            //Server settings
//            $mail->SMTPDebug = 2;                      //启用详细调试输出
            $mail->isSMTP();                                            //使用SMTP
            $mail->Host       = 'smtp.qq.com';                     //将SMTP服务器设置为通过
            $mail->SMTPAuth   = true;                                   //启用SMTP验证
            $mail->Username   = '1956694751@qq.com';                     // SMTP用户名
            $mail->Password   = 'latgklkiojvfeicj';                               // SMTP密码
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         //启用TLS加密；`的PHPMailer :: ENCRYPTION_SMTPS`鼓励
            $mail->Port       = 465;                                    //要连接的TCP端口，在上面的PHPMailer :: ENCRYPTION_SMTPS中使用465

            //Recipients
            $mail->setFrom('1956694751@qq.com',$name);
            $mail->addAddress($email);               //Name is optional
//            $mail->addReplyTo('info@example.com', 'Information');
//            $mail->addCC('cc@example.com');
//            $mail->addBCC('bcc@example.com');

            //Attachments
//            $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
//            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $title;
            $mail->Body    = $content;

            $mail->send();
            return true;
        } catch (\Exception $e) {
           return false;
        }
    }

    /**
     * 发送邮件
     * @param SendEmailByCustomeParamBean $sendEmailByCustomeParamBean
     * @return bool
     * @throws SendEmailException
     */
    public static function sendEmailByCustome(SendEmailByCustomeParamBean $sendEmailByCustomeParamBean)
    {
        if(!$sendEmailByCustomeParamBean->getTitle()){
            throw new SendEmailException("缺少参数");
        }
        if(!$sendEmailByCustomeParamBean->getContent()){
            throw new SendEmailException("缺少参数");
        }
        if(!$sendEmailByCustomeParamBean->getEmail()){
            throw new SendEmailException("缺少参数");
        }
        if(!$sendEmailByCustomeParamBean->getName()){
            throw new SendEmailException("缺少参数");
        }
        if(!$sendEmailByCustomeParamBean->getServiceName()){
            throw new SendEmailException("缺少参数");
        }
        if(!$sendEmailByCustomeParamBean->getServicePassword()){
            throw new SendEmailException("缺少参数");
        }
        $mail = new PHPMailer(true);

        try {
            //Server settings
//            $mail->SMTPDebug = 2;                      //启用详细调试输出
            $mail->isSMTP();                                            //使用SMTP
            $mail->Host       = 'smtp.qq.com';                     //将SMTP服务器设置为通过
            $mail->SMTPAuth   = true;                                   //启用SMTP验证
            $mail->Username   = $sendEmailByCustomeParamBean->getServiceName();                     // SMTP用户名
            $mail->Password   = $sendEmailByCustomeParamBean->getServicePassword();                               // SMTP密码
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         //启用TLS加密；`的PHPMailer :: ENCRYPTION_SMTPS`鼓励
            $mail->Port       = 465;                                    //要连接的TCP端口，在上面的PHPMailer :: ENCRYPTION_SMTPS中使用465

            //Recipients
            $mail->setFrom($sendEmailByCustomeParamBean->getServiceName(),$sendEmailByCustomeParamBean->getName());
            $mail->addAddress($sendEmailByCustomeParamBean->getEmail());               //Name is optional
//            $mail->addReplyTo('info@example.com', 'Information');
//            $mail->addCC('cc@example.com');
//            $mail->addBCC('bcc@example.com');

            //Attachments
//            $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
//            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $sendEmailByCustomeParamBean->getTitle();
            $mail->Body    = $sendEmailByCustomeParamBean->getContent();

            $mail->send();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}
