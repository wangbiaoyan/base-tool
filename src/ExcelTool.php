<?php


namespace Qingrong\Tool;


use Qingrong\Tool\Bean\Excel\ExportManySheetToExcelParamBean;
use Qingrong\Tool\Excel\Export;
use Qingrong\Tool\Exception\FileException;

class ExcelTool
{

    /**
     * 导出csv文件
     * @param array $headers 列名
     * @param array $datas 数据
     * @param string $fileName 文件名
     */
    public static function exportToCsv($headers, $datas, $fileName)
    {
        $export = new Export();
        $export->exportToCsv($headers, $datas, $fileName);
    }

    /**
     * 导出excel文件
     * @param array $headers 列名
     * @param array $datas 数据
     * @param string $fileName 文件名
     */
    public static function exportToExcel($headers, $datas, $fileName)
    {
        $export = new Export();
        $export->exportToExcel($headers, $datas, $fileName);
    }

    /**
     * 获取csv内容
     * @param string $filePath 文件内容
     */
    public static function importCsv($filePath)
    {
        $handle = fopen($filePath, "r");
        if (!$handle) {
            throw new FileException("文件打开失败");
        }
        $list = [];
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);
            for ($c = 0; $c < $num; $c++) {
                $arr[] = $data[$c];
            }
            $list[] = $arr;
            unset($arr);
        }
        fclose($handle);
        return $list;
    }

    /**
     * 获取excel的内容
     * @param $filePath
     */
    public static function importExcel($filePath)
    {
        $export = new Export();
        return $export->importExcel($filePath);
    }

    /**
     * 导出多个sheet的excel
     * @param ExportManySheetToExcelParamBean $exportManySheetToExcelParamBean
     */
    public static function exportManySheetToExcel(ExportManySheetToExcelParamBean $exportManySheetToExcelParamBean)
    {
        $export = new Export();
        $export->exportManySheetToExcel($exportManySheetToExcelParamBean);
    }

    /**
     *
     * @param string $name
     * @param string $path
     */
    public static function exportFilePathObj(string $name, string $path = '')
    {
        $export = \Qingrong\Tool\Export\Export::getExportObj($name, $path);
        return $export;
    }
}
