<?php


namespace Qingrong\Tool\Qr;

use lrq\qrcode\QrcodeMain;
use tekintian\TekinQR;
use Qingrong\Tool\Bean\Qr\CreateQrLogoImgServiceParamBean;
use Zxing\QrReader;

/**
 * 二维码服务类
 * Class QrService
 * @package Tool\Qr
 */
class QrService
{
    /**
     * 获取二维码的内容
     * @param $imgPath string 二维码地址
     */
    public static function getQrContentService($imgPath)
    {
        $qrCode = new QrReader($imgPath);
        return $qrCode->text();
    }

    /**
     * 创建二维码图片
     * @param string $content 二维码内容
     * @param string $fileName 文件名称(带上后缀名.png)
     */
    public static function createQrImgService(string $content,string $fileName)
    {
        $qrcode = new QrcodeMainImp($content);
        $qrcode->execute();
        return $qrcode->png($fileName);
    }


    /**
     * 创建带有logo的二维码
     * @param CreateQrLogoImgServiceParamBean $createQrLogoImgServiceParamBean
     * @return string|null
     */
    public static function createQrLogoImgService(CreateQrLogoImgServiceParamBean $createQrLogoImgServiceParamBean)
    {
        return TekinQR::getQRImg(
            $createQrLogoImgServiceParamBean->getQrContent(),
            $createQrLogoImgServiceParamBean->getQrSize(),
            $createQrLogoImgServiceParamBean->getLogoName(),
            2,
            $createQrLogoImgServiceParamBean->getFileName()
        );
    }
}
