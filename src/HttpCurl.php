<?php


namespace Qingrong\Tool;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Qingrong\Tool\Exception\HttpCustomeException;

class HttpCurl
{
    /**
     * @var int 超时时间
     */
    static $timeOut = 180;

    /**
     * @var int 连接超时时间
     */
    static $connectTimeOut = 10;

    /**
     * get请求
     * @param $url
     * @param $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getCurl($url,$params=[])
    {
        return self::sendRequest($url, 'GET', $params);
    }

    /**
     * get请求原始数据返回
     * @param $url
     * @param $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getCurlOrigin($url,$params=[])
    {
        return self::sendRequest($url, 'GET', $params,false);
    }

    /**
     * post请求
     * @param $url
     * @param $options
     * @param $isJson
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function postCurl($url, $options,$isJson)
    {
        if (isset($options['query'])) {
            if ($isJson) {
                $options['body'] = json_encode($options['query'], JSON_UNESCAPED_UNICODE);
                isset($options['headers'])
                    ?
                    $options['headers']['Content-Type'] = 'application/json'
                    :
                    $options['headers'] = [ 'Content-Type' => 'application/json' ];
            } else {
                $options['form_params'] = $options['query'];
            }

            unset($options['query']);
        }
        return self::sendRequest($url, 'POST', $options);
    }

    /**
     * post请求原始数据返回
     * @param $url
     * @param $options
     * @param $isJson
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function postCurlOrigin($url, $options,$isJson)
    {
        if (isset($options['query'])) {
            if ($isJson) {
                $options['body'] = json_encode($options['query'], JSON_UNESCAPED_UNICODE);
                isset($options['headers'])
                    ?
                    $options['headers']['Content-Type'] = 'application/json'
                    :
                    $options['headers'] = [ 'Content-Type' => 'application/json' ];
            } else {
                $options['form_params'] = $options['query'];
            }

            unset($options['query']);
        }
        return self::sendRequest($url, 'POST', $options,false);
    }

    /**
     * PUT请求
     * @param $url
     * @param array $options
     * @return mixed|void
     */
    public static function put($url, $options = [])
    {
        return self::sendRequest($url, 'PUT', $options);
    }

    public static function getOptions($params)
    {
        $baseOptions = [
            'connect_timeout' => self::$connectTimeOut,
            'timeout'         => self::$timeOut,
            'verify'          => false,
            'debug'           => false,
        ];
        $options = array_merge($baseOptions, $params);
        return $options;
    }

    /**
     * @param $method 请求方式
     * @param $url 请求地址
     * @param $options 请求参数
     * @param $isJsonDecode 是否json解码返回
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function sendRequest($url,$method, $options,$isJsonDecode=true)
    {
        $options = self::getOptions($options);
        $client = new Client();
        $request = $client->request($method, $url, $options);
        $body = $request->getBody();
        $response = $body->getContents();
        if ($isJsonDecode){
            $response = json_decode($response, true);
        }
        return $response;

    }

    /**
     * 后端转发上传文件
     * @param string $url
     * @param string $filepath
     */
    public static function uploadFile(string $url,string $filepath)
    {

        $file = new \CURLFile($filepath,"application/octet-stream");
        $data = [
            "media"=>$file
        ];
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch,CURLOPT_HTTPHEADER,["multipart/form-data"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
        $result = curl_exec($ch);
        if(curl_errno($ch)){
            curl_close($ch);
            throw new HttpCustomeException("文件上传失败");
        }
        curl_close($ch);
        $res = json_decode($result,1);
        if($res["errcode"]==0){
            return $res["media_id"];
        }
    }
}
