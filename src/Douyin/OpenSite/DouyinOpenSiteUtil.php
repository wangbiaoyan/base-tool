<?php

namespace Qingrong\Tool\Douyin\OpenSite;

use Qingrong\Tool\Bean\Douyin\OpenSite\CreateVideoParamBean;
use Qingrong\Tool\Exception\DouyinException;
use Qingrong\Tool\HttpCurl;

/**
 * 抖音开放平台(移动网站应用工具类)
 */
class DouyinOpenSiteUtil
{
    static $baseUrl = "https://open.douyin.com/";

    /**
     * 获取抖音授权地址
     * @param string $source 授权来源
     * @param string $scope 授权作用域
     * @param string $redirectUrl 回调地址
     * @param string $query 回调参数
     * @return string 授权地址
     * @throws DouyinException
     */
    public static function getAuthUrl(string $source,string $scope,string $redirectUrl,string $query)
    {
        $clientKey = config("douyin.client_key");
        switch ($source){
            case "web":
                return "https://open.douyin.com/platform/oauth/connect?client_key={$clientKey}&response_type=code&scope={$scope}&redirect_uri={$redirectUrl}&state={$query}";
                break;
            default:
                throw new DouyinException("当前授权来源不支持");
        }
    }

    /**
     * 获取 access_token
     * @param string $code 授权码
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getAccessToken(string $code)
    {
        $clientSecret = config("douyin.client_secret");
        $clientKey = config("douyin.client_key");
        $url = "oauth/access_token/";
        $params = [
            "client_secret"=>$clientSecret,
            "code"=>$code,
            "grant_type"=>"authorization_code",
            "client_key"=>$clientKey
        ];

        return HttpCurl::postCurl(self::$baseUrl.$url,["query"=>$params],true);
    }

    /**
     * 刷新 refresh_token
     * @param string $refreshToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function renewRefreshToken(string $refreshToken)
    {
        $clientKey = config("douyin.client_key");
        $url = "oauth/renew_refresh_token/";
        $params = [
            "refresh_token"=>$refreshToken,
            "client_key"=>$clientKey
        ];

        return HttpCurl::postCurl(self::$baseUrl.$url,["query"=>$params],true);
    }

    /**
     * 生成 client_token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getClientToken()
    {
        $clientSecret = config("douyin.client_secret");
        $clientKey = config("douyin.client_key");
        $url = "oauth/client_token/";
        $params = [
            "client_secret"=>$clientSecret,
            "grant_type"=>"client_credential",
            "client_key"=>$clientKey
        ];

        return HttpCurl::postCurl(self::$baseUrl.$url,["query"=>$params],true);
    }

    /**
     * 获取 open_ticket
     * @param string $clientToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getOpenTicket(string $clientToken)
    {
        $url = "open/getticket/";
        $params = [
            "headers"=>[
                "access-token"=>$clientToken
            ]
        ];
        return HttpCurl::postCurl(self::$baseUrl.$url,$params,true);
    }

    /**
     * 刷新用户accessToken
     * @param string $refreshToken
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function refreshToken(string $refreshToken)
    {
        $clientKey = config("douyin.client_key");
        $url = "oauth/refresh_token/";
        $params = [
            "refresh_token"=>$refreshToken,
            "grant_type"=>"refresh_token",
            "client_key"=>$clientKey
        ];

        return HttpCurl::postCurl(self::$baseUrl.$url,["query"=>$params],true);
    }

    /**
     * 获取用户公开信息
     * @param string $accessToken 授权token
     * @param string $openId openId
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getOauthUserInfo(string $accessToken,string $openId)
    {
        $url = "oauth/userinfo/";
        $params = [
            "access_token"=>$accessToken,
            "open_id"=>$openId
        ];
        return HttpCurl::postCurl(self::$baseUrl.$url,["query"=>$params],true);
    }

    /**
     * 手机号解密
     * @param string $encryptedMobile
     * @return false|string
     */
    public static function mobileDecrypt( string $encryptedMobile) {
        $clientSecret = config("douyin.client_secret");
        $iv = substr($clientSecret, 0, 16);
        return openssl_decrypt($encryptedMobile, 'aes-256-cbc', $clientSecret, 0, $iv);
    }

    /**
     * 上传视频
     * @param string $accessToken 授权token
     * @param string $openId openId
     * @param \CURLFile $videoFile 视频文件
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function uploadVideo(string $accessToken, string $openId, \CURLFile $videoFile)
    {
        $url = "api/douyin/v1/video/upload_video/";
        $fullUrl = self::$baseUrl.$url."?open_id=".$openId;
        $params = [
            "headers"=>[
                "content-type"=>"multipart/form-data",
                "access-token"=>$accessToken
            ],
            "query"=>[
                "video"=>$videoFile
            ]
        ];
        return HttpCurl::postCurl($fullUrl,$params,true);
    }

    /**
     * 创建视频
     * @param CreateVideoParamBean $createVideoParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createVideo(CreateVideoParamBean $createVideoParamBean)
    {
        $url = "api/douyin/v1/video/create_video/";
        $fullUrl = self::$baseUrl.$url."?open_id=".$createVideoParamBean->getOpenId();

        $params = [
            "video_id"=>$createVideoParamBean->getVideoId(),
        ];
        if ($createVideoParamBean->getText()){
            $params["text"] = $createVideoParamBean->getText();
        }
        if ($createVideoParamBean->getAtUsers()){
            $params["at_users"] = $createVideoParamBean->getAtUsers();
        }

        return HttpCurl::postCurl($fullUrl,["query"=>$params],true);
    }
}