<?php


namespace Qingrong\Tool\Sms;


class ChuanglanSmsSend implements SmsSendInterface
{

    /**
     * @param $mobile 手机号
     * @param $msg 短信内容
     * @param string $needstatus 是否需要状态报告
     * @return mixed
     */
    public function sendSMS($mobile, $msg, $needstatus = 'true')
    {
        //创蓝接口参数
        $postArr = array(
            'account' => config('sms.chuanglan.account'),
            'password' => config('sms.chuanglan.password'),
            'msg' => urlencode($msg),
            'phone' => $mobile,
            'report' => $needstatus
        );

        $result = $this->curlPost(config('sms.chuanglan.api_send_url'), $postArr);
        return $result;
    }

    /**
     * 发送变量短信
     * @param $msg 短信内容
     * @param $params
     * @return mixed
     */
    public function sendVariableSMS($msg, $params)
    {
        //创蓝接口参数
        $postArr = array(
            'account' => config('sms.account'),
            'password' => config('sms.password'),
            'msg' => $msg,
            'params' => $params,
            'report' => 'true'
        );

        $result = $this->curlPost(config('sms.api_send_url'), $postArr);
        return $result;
    }

    /**
     * 查询余额
     * @return mixed
     */
    public function queryBalance()
    {
        //查询参数
        $postArr = array(
            'account' => config('sms.account'),
            'password' => config('sms.password'),
        );
        $result = $this->curlPost(config('sms.api_send_url'), $postArr);
        return $result;
    }


    /**
     * 通过CURL发送HTTP请求
     * @param string $url //请求URL
     * @param array $postFields //请求参数
     * @return mixed
     */
    public function curlPost($url, $postFields)
    {
        $postFields = json_encode($postFields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8'
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $ret = curl_exec($ch);
        if (false == $ret) {
            $result = curl_error($ch);
        } else {
            $rsp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 != $rsp) {
                $result = "请求状态 " . $rsp . " " . curl_error($ch);
            } else {
                $result = $ret;
            }
        }
        curl_close($ch);
        return $result;
    }
}
