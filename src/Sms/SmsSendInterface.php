<?php


namespace Qingrong\Tool\Sms;


interface  SmsSendInterface
{
    /**
     * @param $mobile 手机号
     * @param $msg 短信内容
     * @param string $needstatus 是否需要状态报告
     * @return mixed
     */
    public function sendSMS($mobile, $msg, $needstatus = 'true');

    /**
     * 发送变量短信
     * @param $msg 短信内容
     * @param $params
     * @return mixed
     */
    public function sendVariableSMS($msg, $params);

    /**
     * 查询余额
     * @return mixed
     */
    public function queryBalance();

}
