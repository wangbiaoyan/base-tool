<?php


namespace Qingrong\Tool\Enum;

/**
 * 工具类的redis键
 * Class ToolRedisKeyEnum
 * @package Qingrong\Tool\Enum
 */
class ToolRedisKeyEnum
{
    const DOUYIN_CLIENT_TOKEN = 'tool_douyin_client_token_%s'; // 抖音acess token

    const DOUYIN_ACCESS_TOKEN = 'tool_douyin_access_token_%s'; // 抖音client token
}
