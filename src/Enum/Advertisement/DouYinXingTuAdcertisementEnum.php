<?php

namespace Qingrong\Tool\Enum\Advertisement;

/**
 * 抖音星图广告枚举类型
 */
class DouYinXingTuAdcertisementEnum
{
    /**
     * 激活
     */
    const EVENT_TYPE_0 = 0;

    /**
     * 注册
     */
    const EVENT_TYPE_1 = 1;

    /**
     * 付费
     */
    const EVENT_TYPE_2 = 2;
}