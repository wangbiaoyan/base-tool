<?php

namespace Qingrong\Tool\Enum\Advertisement;

/**
 * 巨量广告枚举类型
 */
class DouYinJuLiangAdvertisementEnum
{
    /**
     * 付费
     */
    const EVENT_TYPE_2 = 2;
    /**
     * 表单
     */
    const EVENT_TYPE_3 = 3;
    /**
     * 有效咨询
     */
    const EVENT_TYPE_5 = 5;
    /**
     * 电话拨打
     */
    const EVENT_TYPE_7 = 7;
    /**
     * 有效获客
     */
    const EVENT_TYPE_19 = 19;
    /**
     * 回访_信息确认
     */
    const EVENT_TYPE_194 = 194;
    /**
     * 回访_加为好友
     */
    const EVENT_TYPE_195 = 195;
    /**
     * 回访_高潜成交
     */
    const EVENT_TYPE_196 = 196;
    /**
     * 支付_存在意向
     */
    const EVENT_TYPE_218 = 218;
    /**
     * 微信_添加企业微信
     */
    const EVENT_TYPE_386 = 386;
    /**
     * 微信_用户首次消息
     */
    const EVENT_TYPE_387 = 387;
    /**
     * 微信_用户首次消息
     */
    const EVENT_TYPE_388 = 388;
    /**
     * 企业微信_取消好友
     */
    const EVENT_TYPE_396 = 396;
}