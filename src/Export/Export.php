<?php


namespace Qingrong\Tool\Export;


use Qingrong\Tool\Exception\ExcelException;
use ZipArchive;

class Export
{
    protected static $export;
    protected $name;
    protected $sheet = [];
    protected $path = "public/storage/exports/";
    protected $childrenFilePath;
    protected $childrenFileNum = 0;
    protected $newSheet = false;

    private function __construct(string $name, string $path = '')
    {
        $this->childrenFilePath = $this->path . 'children/';
        $this->setName($name);
        $this->clearChildrenFile($this->path);
        if ($path) {
            $this->path = $path;
        }
    }

    private function clone()
    {
    }

    public static function getExportObj(string $name, string $path = '')
    {
        if (!self::$export) {
            self::$export = new self($name, $path);
        }
        return self::$export;
    }

    /***
     *设置保存路径
     * @param string $path
     * @throws \Exception
     */
    public function setPath(string $path)
    {
        if (!$this->name) {
            $path = rtrim($path, "/");
            $this->path = $path . "/";
        }
        throw new ExcelException("设置保存路径");
    }

    /***
     * 设置文件名称
     * @param string $name
     */
    private function setName(string $name)
    {
        $this->name = $name;
    }

    /***
     *  获取文件名称
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /***
     * 设置sheet
     * @param string $name
     * @param null $callback
     */
    public function setSheet(string $name, $callback = null)
    {
        $this->newSheet = false;
        if (!in_array($name, $this->sheet)) {
            $this->sheet[] = $name;
            $this->newSheet = true;
        }
        if (is_callable($callback)) {
            call_user_func($callback, self::$export, $name);
        }
        return self::$export;
    }

    /***
     * 设置数据
     * @param array $rows
     * @param string $sheetName
     * @param array $titleArr
     */
    public function setRow(array $rows, string $sheetName, array $titleArr = [])
    {
        if ($this->newSheet && !empty($titleArr)) {
            if (count($titleArr) == count($titleArr, 1)) {    //一维数组
                $titleArr = [ $titleArr ];
            }
            $rows = array_merge($titleArr, $rows);
        }
        if (empty($rows)) return;
        $childrenFilePath = $this->childrenFilePath;
        if (!file_exists($childrenFilePath)) {
            mkdir($childrenFilePath);
        }
        $this->childrenFileNum++;
        $fileName = md5($this->name . $sheetName) . '_' . $this->childrenFileNum . '.json';
        $file = fopen($childrenFilePath . $fileName, 'wr');
        fwrite($file, json_encode($rows, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        fclose($file);
        return self::$export;
    }

    /***
     *  删除历史导出子文件
     * @param $path  文件夹地址
     */
    private function clearChildrenFile($path)
    {
        $childrenFilePath = $this->childrenFilePath;
        if (!file_exists($childrenFilePath)) return;
        if ($childrenDir = @opendir($childrenFilePath)) {
            while ($filename = readdir($childrenDir)) {//遍历目录，读出目录中的文件或文件夹
                if ($filename != '.' && $filename != '..') {//一定要排除两个特殊的目录
                    $subFile = $childrenFilePath . "/" . $filename;//将目录下的文件与当前目录相连
                    if (is_dir($subFile)) {//如果是目录条件则成了
                        $this->clearChildrenFile($subFile);//递归调用自己删除子目录
                    }
                    if (is_file($subFile)) {//如果是文件条件则成立
                        unlink($subFile);//直接删除这个文件
                    }
                }
            }
            closedir($childrenDir);//关闭目录资源
        }
    }

    /***
     * 生成文件
     * @param string $ext
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function create()
    {
        $fileList = [];
        foreach ($this->sheet as $sheet) {
            $fileList[] = $this->createCsv($sheet);
        }

        if (count($this->sheet) == 1) {
            return $fileList[0] ?? [];
        }

        return $this->createZip($fileList);


    }

    /***
     *  生成csv 文件
     * @param $sheet
     * @param string $ext
     * @return array
     */
    private function createCsv($sheet, $ext = 'csv')
    {
        $fileName = $this->path . $sheet . '.' . $ext;
        $childrenFilePath = $this->childrenFilePath;
        $file = fopen($fileName, 'wr');
        for ($i = 1; $i <= $this->childrenFileNum; $i++) {
            $childrenFileName = $childrenFilePath . md5($this->name . $sheet) . '_' . $i . '.json';

            if (!file_exists($childrenFileName)) continue;

            $content = @file_get_contents($childrenFileName);

            $content = json_decode($content, true);


            if (!$content) continue;

            foreach ($content as $row) {
                fputcsv($file, $row);
            }

        }
        fclose($file);

        return compact("fileName", "ext");

    }

    /***
     * 打包文件
     * @param $fileList
     * @param string $ext
     * @return array
     * @throws \Exception
     */
    private function createZip($fileList, $ext = 'zip')
    {
        $objZip = new \ZipArchive();
        $zipFileName = $this->path . $this->name . '.' . $ext;
        $objZip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        foreach ($fileList as $item) {
            // 将文件加入zip对象 传入第二个参数是避免出现目录层级的问题
            $fileName = explode('/', $item['fileName']);
            $fileName = end($fileName);
//            dd($fileName);
            $objZip->addFile($item['fileName'], $fileName);
        }
        $objZip->close();
        foreach ($fileList as $item) {
            unlink($item['fileName']);
        }
        return [
            'fileName' => $zipFileName,
            'ext'      => $ext
        ];
    }
}
