<?php

namespace Qingrong\Tool;

use Qingrong\Tool\Feishu\FeiShu;

/**
 * 飞书消息类
 * Class FeishuTool
 * @package Tool
 */
class FeishuTool
{

    /**
     * 发送消息
     * @param $open_id //type=1传openid，type=2传群id
     * @param $sendData
     * @param $type 0=个人,1=群
     * @return array|false
     * @User liuhaibei
     * @DateTime 2023/06/09 16:05
     */
    public static function sendMessage($token, $open_id, $sendData, $type = 0)
    {
        return (new FeiShu())->setToken($token)->sendMessage($open_id, $sendData, $type);
    }

    /**
     * 获取机器人所在群信息
     * @param $token
     * @param $phone
     * @return false|mixed
     * @User liuhaibei
     * @DateTime 2023/06/09 15:47
     */
    public static function getChatid($token)
    {
        return (new FeiShu())->setToken($token)->getChatid();
    }

    /**
     * 获取用户openid
     * @param $token
     * @param $phone
     * @return false|mixed
     * @User liuhaibei
     * @DateTime 2023/06/09 15:51
     */
    public static function getOpenid($token, $phone = [])
    {
        return (new FeiShu())->setToken($token)->getOpenid($phone);
    }

    /**
     * 获取token
     * @param $app_id
     * @param $app_secret
     * @return false|mixed
     * @User liuhaibei
     * @DateTime 2023/06/09 15:07
     */
    public static function getToken($app_id, $app_secret)
    {
        return (new FeiShu())->setAppId($app_id)->setAppSecret($app_secret)->getToken();
    }
}