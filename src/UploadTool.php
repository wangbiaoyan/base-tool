<?php


namespace Qingrong\Tool;

use Qingrong\Tool\Bean\Oss\GetPrivateDownloadUrlParamBean;
use Qingrong\Tool\Exception\OssException;
use Qingrong\Tool\Oss\HuoshanOss;
use Qiniu\Auth;
use Qiniu\Config;
use Qiniu\Storage\UploadManager;
use Qiniu\Zone;
use SplFileInfo;
use Qingrong\Tool\Bean\Oss\UploadFileParamBean;
use Qingrong\Tool\Exception\FileException;
use Qingrong\Tool\Oss\QiniuOss;

/**
 * 上传工具类
 * Class UploadTool
 * @package Tool
 */
class UploadTool
{

    /**
     * 文件上传接口
     * @param SplFileInfo $file
     * @param string $filename
     * @return string
     * @throws FileException
     */
    public static function uploadFile(UploadFileParamBean $uploadFileParamBean)
    {
        switch (config("oss.default")){
            case "qiniu":
                return (new QiniuOss(config("oss.qiniu.accessKey"),config("oss.qiniu.secretKey")))->uploadFile($uploadFileParamBean);
                break;
            default:
                throw new OssException("oss存储类不存在,请确认");
                break;
        }
    }

    /**
     * 获取私有地址(带过期时间)
     * @param GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean
     * @return mixed
     * @throws OssException
     */
    public static function getPrivateDownloadUrl(GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean)
    {
        switch (config("oss.default")){
            case "qiniu":
                return (new QiniuOss(config("oss.qiniu.accessKey"),config("oss.qiniu.secretKey")))->getPrivateDownloadUrl($getPrivateDownloadUrlParamBean);
                break;
            case "huoshan":
                $client = new HuoshanOss(config("oss.huoshan.accessKey"),config("oss.huoshan.secretKey"));
                $client->setRegion(config("oss.huoshan.region"));
                return $client->getPrivateDownloadUrl($getPrivateDownloadUrlParamBean);
                break;
            default:
                throw new OssException("oss存储类不存在,请确认");
                break;
        }
    }

    /**
     * 获取上传token
     * @return string
     * @throws OssException
     */
    public static function getUploadToken()
    {
        switch (config("oss.default")){
            case "qiniu":
                return (new QiniuOss(config("oss.qiniu.accessKey"),config("oss.qiniu.secretKey")))->getUploadToken(config("oss.qiniu.bucket"));
                break;
            default:
                throw new OssException("oss存储类不存在,请确认");
                break;
        }
    }

}
