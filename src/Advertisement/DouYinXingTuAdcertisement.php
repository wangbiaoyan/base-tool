<?php

namespace Qingrong\Tool\Advertisement;

use Qingrong\Tool\Bean\Advertisement\DouYinXingTuAdcertisement\WechatMiniAppletUploadParamBean;
use Qingrong\Tool\Enum\Advertisement\DouYinXingTuAdcertisementEnum;
use Qingrong\Tool\HttpCurl;

/**
 * 抖音星图广告
 */
class DouYinXingTuAdcertisement
{
    /**
     * 微信小程序数据回传
     * @param WechatMiniAppletUploadParamBean $miniAppletUploadParamBean
     * @return void
     */
    public static function wechatMiniAppletUpload(WechatMiniAppletUploadParamBean $miniAppletUploadParamBean)
    {
        $baseUrl = "https://ad.oceanengine.com/track/activate/?";
        $params = [
            "callback"=>$miniAppletUploadParamBean->getClickId(),
            "event_type"=>$miniAppletUploadParamBean->getEventType()
        ];

        //判断是否付费场景
        if ($miniAppletUploadParamBean->getEventType()==DouYinXingTuAdcertisementEnum::EVENT_TYPE_2){
            $params["props"] = urlencode(json_encode($miniAppletUploadParamBean->getProps()));
        }

        $url = $baseUrl.http_build_query($params);
        return HttpCurl::getCurl($url);
    }
}