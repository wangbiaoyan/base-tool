<?php

namespace Qingrong\Tool\Advertisement;

use Qingrong\Tool\Bean\Advertisement\DouYinJuLiangAdvertisement\ApiClueUploadParamBean;
use Qingrong\Tool\Bean\Advertisement\DouYinJuLiangAdvertisement\EventConversionUploadParamBean;
use Qingrong\Tool\HttpCurl;

/**
 * 抖音巨量广告
 */
class DouYinJuLiangAdvertisement
{
    /**
     * 线索-API上报数据(https://open.oceanengine.com/labels/7/docs/1696710647473167)
     * @param ApiClueUploadParamBean $apiClueUploadParamBean
     * @return void
     */
    public static function ApiClueUpload(ApiClueUploadParamBean $apiClueUploadParamBean){
        $baseUrl = "https://ad.oceanengine.com/track/activate/?";
        $params = [
            "link"=>$apiClueUploadParamBean->getLink(),
            "source"=>$apiClueUploadParamBean->getSource(),
            "conv_time"=>$apiClueUploadParamBean->getConvTime(),
            "event_type"=>$apiClueUploadParamBean->getEventType()
        ];
        $url = $baseUrl.http_build_query($params);
        return HttpCurl::getCurl($url);
    }

    /**
     * 归因方式事件上报(https://open.oceanengine.com/labels/7/docs/1754718886606851)
     * @param EventConversionUploadParamBean $eventConversionUploadParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function eventConversionUpload(EventConversionUploadParamBean $eventConversionUploadParamBean)
    {
        $baseUrl = "https://analytics.oceanengine.com/api/v2/conversion";
        $params = [
            "event_type"=>$eventConversionUploadParamBean->getEventType(),
            "context"=>$eventConversionUploadParamBean->getContext(),
            "timestamp"=>$eventConversionUploadParamBean->getTimestamp()
        ];
        if ($eventConversionUploadParamBean->getEventWeight()){
            //事件权重
            $params["event_weight"] = $eventConversionUploadParamBean->getEventWeight();
        }

        if ($eventConversionUploadParamBean->getProperties()){
            //附加属性
            $params["properties"] = $eventConversionUploadParamBean->getProperties();
        }

        if ($eventConversionUploadParamBean->getOuterEventId()){
            //事件回传ID
            $params["outer_event_id"] = $eventConversionUploadParamBean->getOuterEventId();
        }

        return HttpCurl::postCurl($baseUrl,["query"=>$params],true);
    }
}