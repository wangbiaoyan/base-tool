<?php


namespace Qingrong\Tool;


use Qingrong\Tool\Exception\SmsException;
use Qingrong\Tool\Sms\ChuanglanSmsSend;

class SmsTool
{
    /**
     * 短信验证码发送
     * @param $phone string 手机号
     * @param $msg string 信息
     * @return mixed
     */
    public static function sendCode($phone,$msg)
    {
        switch (config("sms.default")){
            case "chuanglan":
                return (new ChuanglanSmsSend())->sendSMS($phone,$msg);
                break;
            default:
                throw new SmsException("短信运营商不正确");
        }
    }
}
