<?php

namespace Qingrong\Tool\YunLog;

use Qingrong\Tool\Bean\YunLog\GetLogParamBean;
use Qingrong\Tool\HttpCurl;

class AliYunYunLog implements YunLogInterface
{
    private $key;// 阿里日志key
    private $secret;// 阿里日志 Secret
    private $project;// 请求的日志地址
    private $database;//数据库地址

    public function __construct()
    {
        $this->key = config("yunlog.aliyun.key");
        $this->secret = config("yunlog.aliyun.secret");
        $this->project = config("yunlog.aliyun.project");
        $this->database = config("yunlog.aliyun.database");
    }


    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param mixed $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function makeSign(array $params)
    {
        $canonicalizedLogHeaders = [      //延签生成参数
            'x-log-apiversion'      => '0.6.0',
            'x-log-signaturemethod' => 'hmac-sha1',
            'x-log-bodyrawsize'     => '0',
        ];
        ksort($canonicalizedLogHeaders);  //字典排列
        $content = '';
        $first = true;
        foreach ($canonicalizedLogHeaders as $key => $value) {
            if (strpos($key, "x-log-") === 0 || strpos($key, "x-acs-") === 0) { // x-log- header
                if ($first) {
                    $content .= $key . ':' . $value;
                    $first = false;
                } else {
                    $content .= "\n" . $key . ':' . $value;
                }
            }
        }

        $canonicalizedLogHeaders = $content;
        $canonicalizedResource = '/logstores/' . $this->database . '?';
        $QUERY_STRING = http_build_query($params);
        $canonicalizedResource = $canonicalizedResource . urldecode($QUERY_STRING);
        $AccessKeySecret = $this->secret; //阿里日志 Secret
        $date = gmdate('D, d M Y H:i:s') . ' GMT';  //获取GMT时间
        $SignString = 'GET' . "\n"
            . "\n"
            . "\n"
            . $date . "\n"
            . $canonicalizedLogHeaders . "\n"
            . $canonicalizedResource;

        $sign = base64_encode(hash_hmac('sha1', $SignString, $AccessKeySecret, true));  //生成验证签名
        return $sign;
    }

    public function getLog(GetLogParamBean $getLogParamBean)
    {
        $aliLogUrl = 'http://' . $this->project . '.cn-hangzhou.log.aliyuncs.com:80/logstores/' . $this->database;  //请求的日志地址
        $param = [   //请求参数
            'from'    => $getLogParamBean->getStartTime(),
            'to'      => $getLogParamBean->getEndTime(),
            'query'   => $getLogParamBean->getQuery(),
            'line'    => $getLogParamBean->getLine(),
            'reverse' => 'true',
            'offset'  => 0,
            'type'    => 'log'
        ];
        ksort($param);  //字典排列
        $url = $aliLogUrl . '?' . http_build_query($param); //日志地址参数拼接

        $date = gmdate('D, d M Y H:i:s') . ' GMT';  //获取GMT时间

        $sign = $this->makeSign($param, $date, $this->database); //获取验证签名

        $AccessKeyId = $this->key;  //阿里日志key

        $headers = array(   //拼接请求头参数
            'Date'=>$date,
            'Host'=>$this->project . '.cn-hangzhou.log.aliyuncs.com',
            'x-log-apiversion'=>'0.6.0',
            'x-log-signaturemethod'=>'hmac-sha1',
            'Authorization'=>"LOG ".$AccessKeyId . ':' . $sign,
            'Content-Length'=> 0,
            'x-log-bodyrawsize'=>0,
            'Content-Type'=>''
        );

        $data = HttpCurl::getCurl($url,["headers"=>$headers]);
        if ($getLogParamBean->getLine() == 1) {//如果只取一条则返回一维数组
            $data = array_shift($data);
        }
        return $data;
    }
}