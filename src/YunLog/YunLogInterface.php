<?php

namespace Qingrong\Tool\YunLog;


use Qingrong\Tool\Bean\YunLog\GetLogParamBean;

/**
 * 云日志接口
 */
interface YunLogInterface
{
    /**
     * 计算签名
     * @param array $params
     * @return mixed
     */
    public function makeSign(array $params);

    public function getLog(GetLogParamBean $getLogParamBean);
}