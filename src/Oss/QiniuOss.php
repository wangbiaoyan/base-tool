<?php


namespace Qingrong\Tool\Oss;


use Qingrong\Tool\Bean\Oss\GetPrivateDownloadUrlParamBean;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Qingrong\Tool\Bean\Oss\UploadFileParamBean;
use Qingrong\Tool\Exception\OssException;

class QiniuOss implements OssInterface
{

    /**
     * @var appid
     */
    private $accessKey;

    /**
     * @var 秘钥
     */
    private $secretKey;

    /**
     * QiniuOss constructor.
     * @param appid $accessKey
     * @param 秘钥 $secretKey
     */
    public function __construct($accessKey, $secretKey)
    {
        $this->accessKey = $accessKey;
        $this->secretKey = $secretKey;
    }

    /**
     * 上传文件
     * @param $file 文件
     * @param $prefix 前缀
     * @return mixed
     */
    public function uploadFile(UploadFileParamBean $uploadFileParamBean)
    {
        $accessKey = $this->accessKey;
        $secretKey = $this->secretKey;
        $bucket = $uploadFileParamBean->getBucket();
        // 构建鉴权对象
        $auth = new Auth($accessKey, $secretKey);
        // 生成上传 Token
        $token = $auth->uploadToken($bucket);
        // 上传到七牛后保存的文件名
        //判断是否存在指定文件名
        if ($uploadFileParamBean->getFileName()) {
            $fileName = $uploadFileParamBean->getFileName();
        } else {
            $fileName = uniqid();
        }
        $key = rtrim($uploadFileParamBean->getPrefix(), '/') . '/' . $fileName . '.' . $uploadFileParamBean->getExtension();
        // 初始化 UploadManager 对象并进行文件的上传。
        $uploadMgr = new UploadManager();
        // 调用 UploadManager 的 putFile 方法进行文件的上传。
        list($res, $err) = $uploadMgr->putFile($token, $key, $uploadFileParamBean->getFilePath());
        if ($err === null) {
            //删除本地图片
            unlink($uploadFileParamBean->getFilePath());
            return [
                "full_path" => $uploadFileParamBean->getDomain() . $res['key'],
                "path"      => $res['key']
            ];
        } else {
            throw new OssException("上传失败");
        }
    }

    /**
     * @param $domain 地址
     * @param $path 存储路径
     * @param $expire 过期时间
     * @return mixed
     */
    public function getPrivateDownloadUrl(GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean)
    {
        $domain = $getPrivateDownloadUrlParamBean->getDomain();
        $path = $getPrivateDownloadUrlParamBean->getPath();
        $expire = $getPrivateDownloadUrlParamBean->getExpireTime();
        $auth = new Auth($this->accessKey, $this->secretKey);
        $signUrl = $auth->privateDownloadUrl($domain . $path, $expire);
        return $signUrl;
    }

    /**
     * 获取上传token
     * @param $bucket
     * @return string
     */
    public function getUploadToken($bucket)
    {
        $accessKey = $this->accessKey;
        $secretKey = $this->secretKey;
        // 构建鉴权对象
        $auth = new Auth($accessKey, $secretKey);
        // 生成上传 Token
        $token = $auth->uploadToken($bucket);
        return $token;
    }
}
