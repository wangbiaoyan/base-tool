<?php


namespace Qingrong\Tool\Oss;


use Qingrong\Tool\Bean\Oss\GetPrivateDownloadUrlParamBean;
use Qingrong\Tool\Bean\Oss\UploadFileParamBean;

interface OssInterface
{
    /**
     * 上传文件
     * @param $file 文件
     * @param $prefix 前缀
     * @return mixed
     */
    public function uploadFile(UploadFileParamBean $uploadFileParamBean);

    /**
     * 获取文件私有地址(带过期时间)
     * @param GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean
     * @return mixed
     */
    public function getPrivateDownloadUrl(GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean);
}
