<?php

namespace Qingrong\Tool\Oss;

use Qingrong\Tool\Bean\Oss\GetPrivateDownloadUrlParamBean;
use Qingrong\Tool\Bean\Oss\UploadFileParamBean;
use Qingrong\Tool\Exception\OssException;
use Volc\Service\Vod\Models\Request\VodGetPlayInfoRequest;
use Volc\Service\Vod\Vod;

class HuoshanOss implements OssInterface
{

    private $secretKey;

    private $accessKey;

    private $region;

    private $domain;

    private $templateId;

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @param $secretKey
     * @param $accessKey
     */
    public function __construct($secretKey, $accessKey)
    {
        $this->secretKey = $secretKey;
        $this->accessKey = $accessKey;
    }


    public function uploadFile(UploadFileParamBean $uploadFileParamBean)
    {
        // TODO: Implement uploadFile() method.
    }

    public function getPrivateDownloadUrl(GetPrivateDownloadUrlParamBean $getPrivateDownloadUrlParamBean)
    {
        // TODO: Implement getPrivateDownloadUrl() method.
        $client = Vod::getInstance($this->getRegion());
        $client->setAccessKey($this->accessKey);
        $client->setSecretKey($this->secretKey);

        $request = new VodGetPlayInfoRequest();
        $request->setVid($getPrivateDownloadUrlParamBean->getPath());
        $request->setSsl(1);
        if (config("oss.huoshan.m3u8")){
            $request->setFormat("hls"); //封装格式。
        }
        
        try {
            $response = $client->getPlayInfo($request);
        } catch (\Exception $e) {
            throw new OssException($e->getMessage());
        }
        if ($response != null && $response->getResponseMetadata() != null && $response->getResponseMetadata()->getError() != null) {
            throw new OssException($response->getResponseMetadata()->getError()->serializeToJsonString());
        } else {
            $responseData = json_decode($response->serializeToJsonString(), 1);
            return $responseData['Result']['PlayInfoList'][0]['MainPlayUrl'];
        }
    }
}