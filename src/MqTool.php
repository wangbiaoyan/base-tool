<?php


namespace Qingrong\Tool;


use Qingrong\Tool\Bean\Mq\ConsumeParamBean;
use Qingrong\Tool\Exception\MqException;
use Qingrong\Tool\Mq\RabbitMq;

class MqTool
{
    /**
     * 向交换机发布消息
     * @param mixed $exchangeName 交换机名称
     * @param mixed $message 消息
     * @param string $routeKey 路由键
     */
    public static function publish($exchangeName, $message = '', $routeKey = '', $singleton = true)
    {
        switch (config("queue.connections.default")) {
            case "rabbitmq":
                $rabbitMq = RabbitMq::getInstance($singleton);
                //声明交换机
                $rabbitMq->declareExchange($exchangeName, AMQP_EX_TYPE_DIRECT, false);
                //推送消息
                $rabbitMq->publish($message, $routeKey);
                break;
            default:
                throw new MqException("未知消息队列");
                break;
        }
    }

    /**
     * 消费消息队列
     * @param ConsumeParamBean $consumeParamBean
     * @throws MqException
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     */
    public static function consume(ConsumeParamBean $consumeParamBean, RabbitMq $rabbitMq)
    {
        switch (config("queue.connections.default")) {
            case "rabbitmq":
                if ($AMQPEnvelope = $rabbitMq->getQueue()->get()) {
                    //消费消息
                    $consumeParamBean->getObject()->bootStart();
                    call_user_func_array($consumeParamBean->getCallback(), [ $AMQPEnvelope, $rabbitMq->getQueue() ]);
                    $consumeParamBean->getObject()->bootEnd();
                } else {
                    $consumeParamBean->getObject()->sleepHandle($AMQPEnvelope);
                    sleep(1);
                }
                break;
            default:
                throw new MqException("未知消息队列");
                break;
        }
    }

}
