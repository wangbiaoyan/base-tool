<?php


namespace Qingrong\Tool\MiniApplet;

use Qingrong\Tool\Bean\MiniApplet\CompleteOrderAndDivideBean;
use Qingrong\Tool\Bean\MiniApplet\CreateOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\MerchantWithdrawParamBean;
use Qingrong\Tool\Bean\MiniApplet\OrderPushParamBean;
use Qingrong\Tool\Bean\MiniApplet\QueryMerchantBalanceParamBean;
use Qingrong\Tool\Bean\MiniApplet\QueryWithdrawOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\RefundOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\SendImMessageParamBean;
use Qingrong\Tool\Enum\ToolRedisKeyEnum;
use Qingrong\Tool\Exception\MiniAppletException;
use Qingrong\Tool\HttpCurl;

/**
 * 抖音小程序
 * Class DouyinMiniApplet
 * @package Qingrong\Tool\MiniApplet
 */
class DouyinMiniApplet implements MiniAppletInterface
{
    const STATUS = [
        0 => [ 'label' => '待支付', 'value' => 0 ],
        1 => [ 'label' => '已支付', 'value' => 1 ],
        2 => [ 'label' => '已退款', 'value' => 6 ],
        3 => [ 'label' => '已取消', 'value' => 2 ],
        4 => [ 'label' => '已超时', 'value' => 2 ],
        5 => [ 'label' => '已核销', 'value' => 4 ],
        6 => [ 'label' => '退款中', 'value' => 5 ],
        7 => [ 'label' => '退款失败', 'value' => 8 ]
    ];

    private $getOpenIdUrl = 'https://developer.toutiao.com/api/apps/v2/jscode2session'; //openid
    private $getTokenUrl = 'https://developer.toutiao.com/api/apps/v2/token';//access_token
    private $beforePayUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/create_order';   //预下单
    private $orderPushUrl = 'https://developer.toutiao.com/api/apps/order/v2/push';   //订单同步
    private $imMessageUrl = 'https://open.douyin.com/im/send/customer/service/msg/';   // im消息发送
    private $refundOrderUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/create_refund';   //订单退款

    /**
     * appid
     */
    private $appId;

    /**
     * secret
     */
    private $secret;

    private $salt;

    private $expires=7000;//过期时间

    /**
     * DouyinMiniApplet constructor.
     * @param appid $appId
     * @param $secret
     */
    public function __construct($appId, $secret,$salt)
    {
        $this->appId = $appId;
        $this->secret = $secret;
        $this->salt = $salt;
    }


    /**
     * 获取调用凭证
     * @param $appid appid
     * @param $secret secret
     * @return mixed
     */
    public function getAccessToken()
    {
        $key = sprintf(ToolRedisKeyEnum::DOUYIN_ACCESS_TOKEN, $this->appId);
        if (!$token = app("redis")->get($key)) {
            $url = 'https://developer.toutiao.com/api/apps/v2/token';
            $data = [
                'query'=>[
                    'appid'    => $this->appId,
                    'secret' => $this->secret,
                    'grant_type'    => 'client_credential'
                ]
            ];

            $response = HttpCurl::postCurl($url,$data,true);

            if ($response['err_no'] != 0) {
                throw new MiniAppletException("获取调用凭证失败,请重试");
            }
            $token = $response['data']['access_token'];
            app("redis")->setex($key, $this->expires, $token); // 将client token存入redis
        }
        return $token;
    }

    /**
     * 获取clientToken
     * @return mixed
     * @throws MiniAppletException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getClientToken()
    {
        $key = sprintf(ToolRedisKeyEnum::DOUYIN_CLIENT_TOKEN, $this->appId);
        if (!$token = app("redis")->get($key)) {
            $url = 'https://open.douyin.com/oauth/client_token/';
            $data = [
                'query'=>[
                    'client_key'    => $this->appId,
                    'client_secret' => $this->secret,
                    'grant_type'    => 'client_credential'
                ]
            ];

            $response = HttpCurl::postCurl($url,$data,true);

            if (!isset($response['data']['access_token'])) {
                throw new MiniAppletException("获取调用凭证失败,请重试");
            }
            $token = $response['data']['access_token'];
            app("redis")->setex($key, 3000, $token); // 将client token存入redis
        }
        return $token;
    }

    /**
     * 小程序登录
     * @param $code login 接口返回的登录凭证
     * @param $anonymousCode login 接口返回的匿名登录凭证
     * @return mixed
     */
    public function getCode2Session($code, $anonymousCode)
    {
        $url = 'https://developer.toutiao.com/api/apps/v2/jscode2session';
        $data = [
            'query'=>[
                'appid'    => $this->appId,
                'secret' => $this->secret,
            ]
        ];
        if ($code){
            $data["query"]["code"] = $code;
        }

        if ($anonymousCode){
            $data["query"]["anonymous_code"] = $anonymousCode;
        }
        $response = HttpCurl::postCurl($url,$data,true);

        return $response;
//
//        if ($response['err_no'] != 0) {
//            throw new MiniAppletException("登录失败,请重试");
//        }
//        return $response['data'];
    }

    /***
     * 请求签名
     * @param $map
     * @return string  抖音提供加签方式
     */
    public function sign($map)
    {
        $salt = $this->salt;
        $rList = array();
        foreach ($map as $k => $v) {
            if ($k == "other_settle_params" || $k == "app_id" || $k == "sign" || $k == "thirdparty_id")
                continue;
            $value = trim(strval($v));
            $len = strlen($value);
            if ($len > 1 && substr($value, 0, 1) == "\"" && substr($value, $len, $len - 1) == "\"")
                $value = substr($value, 1, $len - 1);
            $value = trim($value);
            if ($value == "" || $value == "null")
                continue;
            array_push($rList, $value);
        }
        array_push($rList, $salt);
        sort($rList, 2);
        return md5(implode('&', $rList));
    }

    /**
     * 计算回调的签名
     * @param $token  抖音支付设置的token
     * @param $timestamp 回调的时间戳
     * @param $nonce 回调的随机字符串
     * @param $msg 回调的消息
     */
    public static function callSign($token, $timestamp, $nonce, $msg)
    {
        $params = [$token,$timestamp,$nonce,$msg];
        sort($params,2);
        $str = implode("",$params);
        $replaceStr = str_replace("\\","",$str);
        return sha1($replaceStr);
    }

    public function createOrder(CreateOrderParamBean $createOrderParamBean)
    {
        $data = [
            'app_id'       => $this->appId,
            'out_order_no' => $createOrderParamBean->getOutOrderNo(),
            'total_amount' => $createOrderParamBean->getTotalAmount(),
            'subject'      => $createOrderParamBean->getSubject(),
            'body'         => $createOrderParamBean->getBody(),
            'valid_time'   => $createOrderParamBean->getValidTime(),
            'sign'         => '',
            'notify_url'   => $createOrderParamBean->getNotifyUrl()
        ];

        if ($createOrderParamBean->getCpExtra()){
            //存在自定义字段
            $data["cp_extra"] = $createOrderParamBean->getCpExtra();
        }

        if ($createOrderParamBean->getPayScene()){
            //是否指定充值场景
            $data["pay_scene"] = $createOrderParamBean->getPayScene();
        }
        $data['sign'] = $this->sign($data);
        $response = HttpCurl::postCurl($this->beforePayUrl,["query"=>$data],true);

        return $response;
//        if ($response['err_no'] != 0) {
//            throw new MiniAppletException("抖音订单创建失败!.".$response["err_tips"]??"");
//        }
//
//        return $response['data'];
    }


    /***
     * 订单回传
     * @param OrderPushParamBean $orderPushParamBean
     */
    public function orderPush(OrderPushParamBean $orderPushParamBean)
    {
        $data = [
            "access_token"=>$orderPushParamBean->getAccessToken(),
            "app_name"=>$orderPushParamBean->getAppName(),
            "open_id"=>$orderPushParamBean->getOpenId(),
            "order_detail"=>$orderPushParamBean->getOrderDetail(),
            "order_status"=>$orderPushParamBean->getOrderStatus(),
            "order_type"=>$orderPushParamBean->getOrderType(),
            "update_time"=>$orderPushParamBean->getUpdateTime()
        ];

        if ($orderPushParamBean->getExtra()){
            $data["extra"] = $orderPushParamBean->getExtra();
        }

        $response = HttpCurl::postCurl($this->orderPushUrl,["query"=>$data],true);
        return $response;
    }

    /**
     * 订单退款
     * @param RefundOrderParamBean $refundOrderParamBean
     * @return mixed
     */
    public function refundOrder(RefundOrderParamBean $refundOrderParamBean)
    {
        $data = [
            "app_id"=>$this->appId,
            "out_order_no"=>$refundOrderParamBean->getOutOrderNo(),
            "out_refund_no"=>$refundOrderParamBean->getOutRefundNo(),
            "reason"=>$refundOrderParamBean->getReason(),
            "refund_amount"=>$refundOrderParamBean->getRefundAmount(),
            "notify_url"=>$refundOrderParamBean->getNotifyUrl()
        ];

        if ($refundOrderParamBean->getCpExtra()){
            $data["cp_extra"] = $refundOrderParamBean->getCpExtra();
        }

        $data['sign'] = $this->sign($data);
        $response = HttpCurl::postCurl($this->refundOrderUrl,["query"=>$data],true);
        return $response;
    }

    /**
     * im消息发送
     * @param SendImMessageParamBean $sendImMessageParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendImMessage(SendImMessageParamBean $sendImMessageParamBean)
    {
        $data = [
            "conversion_id"=>$sendImMessageParamBean->getConversionId(),
            "send_url"=>$sendImMessageParamBean->getSendUrl(),
            "to_user_id"=>$sendImMessageParamBean->getToUserId(),
            "msg_type"=>$sendImMessageParamBean->getMsgType()
        ];

        $params = [
            "query"=>$data,
            "headers"=>[
                "access-token"=>$sendImMessageParamBean->getAccessToken()
            ]
        ];
        $url = $this->imMessageUrl."?open_id=".$sendImMessageParamBean->getOpenId();
        $response = HttpCurl::postCurl($url,$params,true);
        return $response;
    }

    /**
     * 结算及分账
     * @param CompleteOrderAndDivideBean $completeOrderAndDivideBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function completeOrderAndDivide(CompleteOrderAndDivideBean $completeOrderAndDivideBean)
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/v1/settle";
        $data = [
            "app_id"=>$this->appId,
            "out_settle_no"=>$completeOrderAndDivideBean->getOutSettleNo(),
            "out_order_no"=>$completeOrderAndDivideBean->getOutOrderNo(),
            "settle_desc"=>$completeOrderAndDivideBean->getSettleDesc()
        ];
        //判断是否存在分账参数
        if ($completeOrderAndDivideBean->getSettleParams()){
            $data["settle_params"] = $completeOrderAndDivideBean->getSettleParams();
        }
        //判断是否存在自定义字段
        if ($completeOrderAndDivideBean->getCpExtra()){
            $data["cp_extra"] = $completeOrderAndDivideBean->getCpExtra();
        }
        //判断是否存在回调地址
        if ($completeOrderAndDivideBean->getNotifyUrl()){
            $data["notify_url"] = $completeOrderAndDivideBean->getNotifyUrl();
        }
        //判断是否存在服务商ID
        if($completeOrderAndDivideBean->getThirdpartyId()){
            $data["thirdparty_id"] = $completeOrderAndDivideBean->getThirdpartyId();
        }
        if ($completeOrderAndDivideBean->getFinish()){
            $data["finish"] = $completeOrderAndDivideBean->getFinish();
        }
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 结算和分账结果查询
     * @param string $outSettleNo 开发者侧分账订单号, 同一小程序下不可重复
     * @param string $thirdpartyId 第三方平台服务商 id，非服务商模式留空
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCompleteOrderAndDevideResult(string $outSettleNo, string $thirdpartyId = "")
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/v1/query_settle";
        $data = [
            "app_id"=>$this->appId,
            "out_settle_no"=>$outSettleNo
        ];
        if ($thirdpartyId){
            $data["thirdparty_id"] = $thirdpartyId;
        }

        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 可分账余额查询
     * @param string $outOrderNo 商户侧支付单号
     * @param string $thirdpartyId 第三方平台服务商 id
     * @param string $outItemOrderNo 商户侧支付子单单号
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryOrderDividePrice(string $outOrderNo,string $thirdpartyId="",string $outItemOrderNo="")
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/v1/unsettle_amount";
        $data = [
            "app_id"=>$this->appId,
            "out_order_no"=>$outOrderNo
        ];
        if ($thirdpartyId){
            $data["thirdparty_id"] = $thirdpartyId;
        }
        if ($outItemOrderNo){
            $data["out_item_order_no"] = $outItemOrderNo;
        }
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 查询商户各渠道账户余额。
     * @param QueryMerchantBalanceParamBean $queryMerchantBalanceParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryMerchantBalance(QueryMerchantBalanceParamBean $queryMerchantBalanceParamBean)
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/saas/query_merchant_balance";
        $data = [
            "app_id"=>$queryMerchantBalanceParamBean->getAppId(),
            "merchant_uid"=>$queryMerchantBalanceParamBean->getMerchantUid(),
            "channel_type"=>$queryMerchantBalanceParamBean->getChannelType()
        ];
        if ($queryMerchantBalanceParamBean->getThirdpartyId()){
            $data["thirdparty_id"] = $queryMerchantBalanceParamBean->getThirdpartyId();
        }
        if ($queryMerchantBalanceParamBean->getMerchantEntity()){
            $data["merchant_entity"] = $queryMerchantBalanceParamBean->getMerchantEntity();
        }
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 商户提现
     * @param MerchantWithdrawParamBean $merchantWithdrawParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function merchantWithdraw(MerchantWithdrawParamBean $merchantWithdrawParamBean)
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/saas/merchant_withdraw";
        $data = [
            "app_id"=>$merchantWithdrawParamBean->getAppId(),
            "merchant_uid"=>$merchantWithdrawParamBean->getMerchantUid(),
            "channel_type"=>$merchantWithdrawParamBean->getChannelType(),
            "withdraw_amount"=>$merchantWithdrawParamBean->getWithdrawAmount(),
            "out_order_id"=>$merchantWithdrawParamBean->getOutOrderId()
        ];
        if ($merchantWithdrawParamBean->getThirdpartyId()){
            $data["thirdparty_id"] = $merchantWithdrawParamBean->getThirdpartyId();
        }
        if ($merchantWithdrawParamBean->getCallback()){
            $data["callback"] = $merchantWithdrawParamBean->getCallback();
        }
        if ($merchantWithdrawParamBean->getCpExtra()){
            $data["cp_extra"] = $merchantWithdrawParamBean->getCpExtra();
        }
        if ($merchantWithdrawParamBean->getMerchantEntity()){
            $data["merchant_entity"] = $merchantWithdrawParamBean->getMerchantEntity();
        }
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 商户提现结果查询
     * @param QueryWithdrawOrderParamBean $queryWithdrawOrderParamBean
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryWithdrawOrder(QueryWithdrawOrderParamBean $queryWithdrawOrderParamBean)
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/ecpay/saas/query_withdraw_order";
        $data = [
            "app_id"=>$queryWithdrawOrderParamBean->getAppId(),
            "merchant_uid"=>$queryWithdrawOrderParamBean->getMerchantUid(),
            "channel_type"=>$queryWithdrawOrderParamBean->getChannelType(),
            "out_order_id"=>$queryWithdrawOrderParamBean->getOutOrderId()
        ];
        if ($queryWithdrawOrderParamBean->getThirdpartyId()){
            $data["thirdparty_id"] = $queryWithdrawOrderParamBean->getThirdpartyId();
        }
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 申请短视频自主挂载能力
     * @param string $reason 理由
     * @return mixed
     * @throws MiniAppletException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function applyCapacity(string $reason)
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/v1/capacity/apply_capacity";
        $data = [
            "headers"=>[
                "access-token"=>$this->getClientToken()
            ],
            "query"=>[
                "capacity_key"=>"video_self_mount",
                "apply_reason"=>$reason
            ]
        ];
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }

    /**
     * 查询短视频自主挂载能力申请状态
     * @return mixed
     * @throws MiniAppletException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryApplyCapacityStatus()
    {
        $baseUrl = "https://developer.toutiao.com/api/apps/v1/capacity/query_apply_status";
        $data = [
            "headers"=>[
                "access-token"=>$this->getClientToken()
            ],
            "query"=>[
                "capacity_key"=>"video_self_mount",
            ]
        ];
        $data["sign"] = $this->sign($data);
        return HttpCurl::postCurl($baseUrl,["query"=>$data],true);
    }
}
