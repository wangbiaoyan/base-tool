<?php


namespace Qingrong\Tool\MiniApplet;

use Qingrong\Tool\Bean\MiniApplet\CreateOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\RefundOrderParamBean;

/**
 * 小程序接口
 * Interface MiniAppletInterface
 * @package Qingrong\Tool\MiniApplet
 */
interface MiniAppletInterface
{
    /**
     * 获取调用凭证
     * @return mixed
     */
    public function getAccessToken();

    /**
     * 小程序登录
     * @param $code login 接口返回的登录凭证
     * @param $anonymousCode login 接口返回的匿名登录凭证
     * @return mixed
     */
    public function getCode2Session($code,$anonymousCode);

    /**
     * 预下单
     * @param CreateOrderParamBean $createOrderParamBean
     * @return mixed
     */
    public function createOrder(CreateOrderParamBean $createOrderParamBean);

    /**
     * 订单退款
     * @param RefundOrderParamBean $refundOrderParamBean
     * @return mixed
     */
    public function refundOrder(RefundOrderParamBean $refundOrderParamBean);
}
