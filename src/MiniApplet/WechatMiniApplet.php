<?php

namespace Qingrong\Tool\MiniApplet;

use Qingrong\Tool\Bean\MiniApplet\CreateOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\RefundOrderParamBean;
use Qingrong\Tool\Exception\MiniAppletException;
use Qingrong\Tool\HttpCurl;

/**
 * 微信官方
 */
class WechatMiniApplet implements MiniAppletInterface
{
    private $jsapiUrl = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";//jsapi预下单地址

    const AUTH_TAG_LENGTH_BYTE = 16;

    /**
     * appid
     */
    private $appId;

    /**
     * secret
     */
    private $secret;


    private $expires=7000;//过期时间

    /**
     * DouyinMiniApplet constructor.
     * @param appid $appId
     * @param $secret
     */
    public function __construct($appId, $secret)
    {
        $this->appId = $appId;
        $this->secret = $secret;
    }

    public function getAccessToken()
    {
        // TODO: Implement getAccessToken() method.

    }

    /**
     * 小程序登录
     * @param $code
     * @param $anonymousCode
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCode2Session($code, $anonymousCode)
    {
        // TODO: Implement getCode2Session() method.
        $url = 'https://api.weixin.qq.com/sns/jscode2session?';
        $data = [
                'appid'    => $this->appId,
                'secret' => $this->secret,
                'js_code'=>$code,
                'grant_type'=>"authorization_code"
        ];
        return HttpCurl::getCurl($url.http_build_query($data),[]);
    }

    /**
     * 创建小程序订单
     * @param CreateOrderParamBean $createOrderParamBean
     * @return mixed|void
     */
    public function createOrder(CreateOrderParamBean $createOrderParamBean)
    {
        $url = $this->jsapiUrl;
        $data = [
            'appid'    => $this->appId,
            'mchid' => $createOrderParamBean->getMchid(),
            "description"=>$createOrderParamBean->getSubject(),
            "out_trade_no"=>$createOrderParamBean->getOutOrderNo(),
            "notify_url"=>$createOrderParamBean->getNotifyUrl(),
            "amount"=>[
                "total"=>$createOrderParamBean->getTotalAmount(),
                "currency"=>"CNY"
            ],
            "payer"=>[
                "openid"=>$createOrderParamBean->getOpenId()
            ]
        ];
        $res = HttpCurl::postCurl($url,["query"=>$data],true);
        if (!isset($res["prepay_id"])){
            throw new MiniAppletException(json_encode($res));
        }
        //获取到预支付单号
        return $this->appletPayParam($this->appId,$res["prepay_id"]);
    }

    public function refundOrder(RefundOrderParamBean $refundOrderParamBean)
    {
        // TODO: Implement refundOrder() method.
    }

    /**
     * 小程序调起支付参数
     * @param $appid
     * @param $perpayId
     */
    private function appletPayParam($appId, $perpayId)
    {
        $timeStamp = time();
        $nonceStr = md5($timeStamp);
        $package = 'prepay_id=' . $perpayId;
        $message = $appId . "\n" .
            $timeStamp . "\n" .
            $nonceStr . "\n" .
            $package . "\n";
        $mch_private_key = file_get_contents(app()->configPath("cert"). '/apiclient_key.pem');
        openssl_sign($message, $raw_sign, $mch_private_key, 'sha256WithRSAEncryption');
        $paySign = base64_encode($raw_sign);
        $signType = 'RSA';
        return [
            "appId"=>$appId,
            "timeStamp"=>$timeStamp,
            "nonceStr"=>$nonceStr,
            "package"=>$package,
            "paySign"=>$paySign,
            "signType"=>$signType
        ];
    }

    /***
     *   回调参数解密
     */
    public static function notifyOrderDecode($request,$apiV3Key)
    {
        $ciphertext = \base64_decode($request['resource']['ciphertext']);
        $associatedData = $request['resource']['associated_data'];
        $nonceStr = $request['resource']['nonce'];
        $aesKey = $apiV3Key;
        if (strlen($ciphertext) <= self::AUTH_TAG_LENGTH_BYTE) {
            return false;
        }
        // ext-sodium (default installed on >= PHP 7.2)
        if (function_exists('\sodium_crypto_aead_aes256gcm_is_available') && \sodium_crypto_aead_aes256gcm_is_available()) {
            $data = \sodium_crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $aesKey);
        }
        // ext-libsodium (need install libsodium-php 1.x via pecl)
        if (function_exists('\Sodium\crypto_aead_aes256gcm_is_available') && \Sodium\crypto_aead_aes256gcm_is_available()) {
            $data = \Sodium\crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $aesKey);
        }
        // openssl (PHP >= 7.1 support AEAD)
        if (PHP_VERSION_ID >= 70100 && in_array('aes-256-gcm', \openssl_get_cipher_methods())) {
            $ctext = substr($ciphertext, 0, -self::AUTH_TAG_LENGTH_BYTE);
            $authTag = substr($ciphertext, -self::AUTH_TAG_LENGTH_BYTE);

            $data = \openssl_decrypt($ctext, 'aes-256-gcm', $aesKey, \OPENSSL_RAW_DATA, $nonceStr,
                $authTag, $associatedData);
        }
        if (!$data) {
            throw new MiniAppletException("AEAD_AES_256_GCM需要PHP 7.1以上或者安装libsodium-php");
        }
        return json_decode($data, true);
    }
}