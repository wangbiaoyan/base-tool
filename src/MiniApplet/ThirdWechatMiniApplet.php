<?php

namespace Qingrong\Tool\MiniApplet;

use Qingrong\Tool\Bean\MiniApplet\CreateOrderParamBean;
use Qingrong\Tool\Bean\MiniApplet\RefundOrderParamBean;
use Qingrong\Tool\Exception\MiniAppletException;
use Qingrong\Tool\HttpCurl;

/**
 * 微信三方
 */
class ThirdWechatMiniApplet implements MiniAppletInterface
{

    private $jsapiUrl = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";//jsapi预下单地址

    /**
     * appid
     */
    private $appId;

    /**
     * secret
     */
    private $secret;

    /**
     * 三方appId
     * @var string $thirdAppId
     */
    private $thirdAppId;

    /**
     * 三方商户号
     * @var string $thirdMchId
     */
    private $thirdMchId;

    /**
     * 三方appkey
     * @var string $thirdAppKey
     */
    private $thirdAppKey;

    /**
     * @return string
     */
    public function getThirdAppKey()
    {
        return $this->thirdAppKey;
    }

    /**
     * @param string $thirdAppKey
     */
    public function setThirdAppKey(string $thirdAppKey)
    {
        $this->thirdAppKey = $thirdAppKey;
    }

    /**
     * @return string
     */
    public function getThirdAppId()
    {
        return $this->thirdAppId;
    }

    /**
     * @param string $thirdAppId
     */
    public function setThirdAppId(string $thirdAppId)
    {
        $this->thirdAppId = $thirdAppId;
    }

    /**
     * @return string
     */
    public function getThirdMchId()
    {
        return $this->thirdMchId;
    }

    /**
     * @param string $thirdMchId
     */
    public function setThirdMchId(string $thirdMchId)
    {
        $this->thirdMchId = $thirdMchId;
    }

    private $expires=7000;//过期时间

    /**
     * DouyinMiniApplet constructor.
     * @param appid $appId
     * @param $secret
     */
    public function __construct($appId, $secret)
    {
        $this->appId = $appId;
        $this->secret = $secret;
    }

    public function getAccessToken()
    {
        // TODO: Implement getAccessToken() method.

    }

    /**
     * 小程序登录
     * @param $code
     * @param $anonymousCode
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCode2Session($code, $anonymousCode)
    {
        // TODO: Implement getCode2Session() method.
        $url = 'https://api.weixin.qq.com/sns/jscode2session?';
        $data = [
                'appid'    => $this->appId,
                'secret' => $this->secret,
                'js_code'=>$code,
                'grant_type'=>"authorization_code"
        ];
        return HttpCurl::getCurl($url.http_build_query($data),[]);
    }

    /**
     * 创建小程序订单
     * @param CreateOrderParamBean $createOrderParamBean
     * @return mixed|void
     */
    public function createOrder(CreateOrderParamBean $createOrderParamBean)
    {
        $url = "https://pay.palmpay.cn/sdkServer/thirdpays/pay/WECHAT_MNPROGRAM";
        $data = [
            "mchId"=>$this->getThirdMchId(),
            "appid"=>$this->getThirdAppId(),
            "version"=>"3.0",
            "money"=>$createOrderParamBean->getTotalAmount(),
            "outTradeNo"=>$createOrderParamBean->getOutOrderNo(),
            "productName"=>$createOrderParamBean->getSubject(),
            "productDesc"=>$createOrderParamBean->getSubject(),
            "subAppid"=>$this->appId,
            "openid"=>$createOrderParamBean->getOpenId(),
            "notifyUrl"=>$createOrderParamBean->getNotifyUrl(),
            "ip"=>$createOrderParamBean->getIp()
        ];
        $data["sign"] = $this->getThirdSign($data);

        return HttpCurl::getCurl($url."?".http_build_query($data));

//        if ($res["errcode"]!=0){
//            throw new MiniAppletException(json_encode($res));
//        }
//        return json_decode($res['result']['pay_info'], true);
    }

    public function refundOrder(RefundOrderParamBean $refundOrderParamBean)
    {
        $url = "https://pay.palmpay.cn/sdkServer/thirdpays/refund";
        $data = [
            "appid"=>$this->getThirdAppId(),
            "mchId"=>$this->getThirdMchId(),
            "pdorderid"=>$refundOrderParamBean->getOutOrderNo(),
            "money"=>$refundOrderParamBean->getRefundAmount(),
            "version"=>"3.0",
            "outRefundNo"=>$refundOrderParamBean->getOutRefundNo(),
            "refundMoney"=>$refundOrderParamBean->getRefundAmount(),
            "refundReason"=>$refundOrderParamBean->getReason(),
        ];
        $data["sign"] = md5($data["mchId"].$data["appid"].$data["pdorderid"].$data["money"].$data["version"].$data["outRefundNo"].$data["refundMoney"].$this->getThirdAppKey());
        return HttpCurl::getCurl($url."?".http_build_query($data));
    }

    /**
     * 查询退款订单状态
     * @param $refundOrderNumber string 退款订单编号
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryRefundOrder($refundOrderNumber)
    {
        $url = "https://pay.palmpay.cn/sdkServer/thirdpays/refundquery";
        $data = [
            "appid"=>$this->getThirdAppId(),
            "mchId"=>$this->getThirdMchId(),
            "outRefundNo"=>$refundOrderNumber,
            "version"=>"3.0"
        ];
        $data["sign"] = md5($data["appid"].$data["mchId"].$data["version"].$this->getThirdAppKey());
        return HttpCurl::getCurl($url."?".http_build_query($data));
    }

    /**
     * 查询订单信息
     * @param $pdorderid string 三方订单编号
     * @param $outTradeNo string 商户订单编号
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryOrder($pdorderid,$outTradeNo)
    {
        $url = "https://pay.palmpay.cn/sdkServer/thirdpayorder";
        $data = [
            "appid"=>$this->getThirdAppId(),
            "mchId"=>$this->getThirdMchId(),
            "version"=>"3.0",
            "pdorderid"=>$pdorderid,
            "outTradeNo"=>$outTradeNo
        ];
        $data["sign"] = md5($data["appid"].$data["pdorderid"].$data["outTradeNo"].$this->getThirdAppKey());
        return HttpCurl::getCurl($url."?".http_build_query($data));
    }

    /**
     * 三方签名
     * @param $data
     * @return string
     */
    private function getThirdSign($data)
    {
        $service = 'WECHAT_MNPROGRAM';
        $appkey = $this->getThirdAppKey();

        return md5($data['appid'] .
            $service .
            $data['money'] .
            $data['outTradeNo'] .
            $appkey);
    }

    /**
     * 回调签名
     * @param array $params
     * @return string
     */
    public static function callSign(array $params,$appKey)
    {
        $tmp = "";
        unset($params["sign"]);
        ksort($params);
        foreach ($params as $key=>$val){
            $tmp .= "&".$key."=".$val;
        }
        $tmp = trim($tmp,"&");
        $tmp .= "&key=".$appKey;
        return md5($tmp);
    }
}