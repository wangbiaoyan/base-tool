<?php


namespace Qingrong\Tool\Exception;


class OssException extends \Exception
{
    public function __construct($msg)
    {
        parent::__construct($msg,500);
    }
}
