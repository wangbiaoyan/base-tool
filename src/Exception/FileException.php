<?php


namespace Qingrong\Tool\Exception;


class FileException extends \Exception
{
    public function __construct($msg)
    {
        parent::__construct($msg,500);
    }
}
