<?php


namespace Qingrong\Tool\Exception;


class SendEmailException extends \Exception
{
    public function __construct($msg)
    {
        parent::__construct($msg,500);
    }
}
